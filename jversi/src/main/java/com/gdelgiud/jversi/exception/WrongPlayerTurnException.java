package com.gdelgiud.jversi.exception;

public class WrongPlayerTurnException extends Exception {

	private static final long serialVersionUID = 7816723846636169248L;
	
	@Override
	public String getMessage() {
		return "It's not this player's turn";
	}

}
