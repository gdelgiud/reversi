package com.gdelgiud.jversi.exception;

public class IllegalMoveException extends InvalidMoveException {

	private static final long serialVersionUID = -1130755752838649526L;
	
	@Override
	public String getMessage() {
		return "No tiles were converted";
	}

}
