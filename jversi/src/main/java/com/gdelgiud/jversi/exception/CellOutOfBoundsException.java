package com.gdelgiud.jversi.exception;

public class CellOutOfBoundsException extends IllegalMoveException {

	private static final long serialVersionUID = -1700493335170519648L;
	
	@Override
	public String getMessage() {
		return "The selected position is outside of the board";
	}

}
