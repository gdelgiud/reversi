package com.gdelgiud.jversi.exception;

public class CellNotEmptyException extends InvalidMoveException {

	private static final long serialVersionUID = 6915811067551571019L;
	
	@Override
	public String getMessage() {
		return "The selected cell is not empty";
	}

}
