package com.gdelgiud.jversi;

import com.gdelgiud.jversi.exception.CellNotEmptyException;
import com.gdelgiud.jversi.exception.CellOutOfBoundsException;
import com.gdelgiud.jversi.exception.IllegalMoveException;
import com.gdelgiud.jversi.exception.InvalidMoveException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Board implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static final int BOARD_WIDTH = 8;
	public static final int BOARD_HEIGHT = 8;
	
	private TileColor[][] board;
	
	private int whiteTiles = 0;
	private int blackTiles = 0;
	
	private List<BoardPosition> possibleWhiteMoves = null;
	private List<BoardPosition> possibleBlackMoves = null;
	
	/**
	 * Creates a board with default tiles placement
	 */
	public Board() {
		reset();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(board);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Board other = (Board) obj;
		if (!Arrays.deepEquals(board, other.board))
			return false;
		return true;
	}

	/**
	 * Copy the given board
	 */
	public Board(Board original) {
		// Copy cached content
		this.whiteTiles = original.getWhiteTiles();
		this.blackTiles = original.getBlackTiles();
		this.possibleWhiteMoves = original.possibleWhiteMoves;
		this.possibleBlackMoves = original.possibleBlackMoves;
		
		// Copy the board
		board = new TileColor[BOARD_WIDTH][BOARD_HEIGHT];
		for(int i = 0; i < BOARD_HEIGHT; i++){
			for(int j = 0; j < BOARD_WIDTH; j++) {
				board[j][i] = original.board[j][i];
			}
            //System.arraycopy(original.board[i], 0, board[i], 0, BOARD_WIDTH);
        }
	}
	
	/**
	 * Sets the board back to default
	 */
	public void reset() {
		board = new TileColor[BOARD_WIDTH][BOARD_HEIGHT];
		
		board[3][3] = TileColor.WHITE;
		board[4][4] = TileColor.WHITE;
		board[3][4] = TileColor.BLACK;
		board[4][3] = TileColor.BLACK;
		
		possibleBlackMoves = null;
		possibleWhiteMoves = null;
		computeTilesAmount();
	}
	
	/**
	 * Places a tile with the given type in the given position. Checks if the move is valid and performs conversions.
	 * @param type The color of the tile to be placed
	 * @param position The position where this tile will be placed
	 * @throws CellOutOfBoundsException If position if outside the board's bounds
	 * @throws CellNotEmptyException If there is already a tile placed in position
	 * @throws IllegalMoveException If the move doesn't follow Reversi's rules
	 * @returns The amount of switched tiles
	 */
	public int playTile(TileColor type, BoardPosition position) throws InvalidMoveException {
		int x = position.x;
		int y = position.y;
		int i = 0;
		int j = 0;
		boolean lineEnded = false;
		TileColor oppositeType;
		
		if ((x < 0 || x >= BOARD_WIDTH) || (y < 0 || y >= BOARD_HEIGHT)) {
			throw new CellOutOfBoundsException();
		}
		
		if (board[x][y] != null) {
			throw new CellNotEmptyException();
		}
		
		if (type == TileColor.WHITE) {
			oppositeType = TileColor.BLACK;
		}
		else {
			oppositeType = TileColor.WHITE;
		}
		
		// While checking each direction, save the tiles to be converted
		// Can't convert more than BOARD_WIDTH tiles per direction
		BoardPosition[] foundEnemyTiles = new BoardPosition[BOARD_WIDTH];
		int totalChangedTiles = 0;
		
		// Check left line
		for (i = 1, j = 0, lineEnded = false; x - i >= 0 && !lineEnded; i++) {
			TileColor currentTileType = board[x - i][y];
			// Add opposing tiles as we check
			if (currentTileType == oppositeType) {
				foundEnemyTiles[j++] = new BoardPosition(x - i, y);
			} else if (currentTileType == type) {
				// We found our matching tile. Convert the tiles we found.
				for (int k = 0; k < j; k++) {
					board[foundEnemyTiles[k].x][foundEnemyTiles[k].y] = type;
				}
				totalChangedTiles += j;
				lineEnded = true;
			} else {
				// We either found an empty space, or something else. We don't
				// convert anything
				lineEnded = true;
			}
		}
		
		// Check right line
		for (i = 1, j = 0, lineEnded = false; x + i < BOARD_WIDTH && !lineEnded; i++) {
			TileColor currentTileType = board[x + i][y];
			// Add opposing tiles as we check
			if (currentTileType == oppositeType) {
				foundEnemyTiles[j++] = new BoardPosition(x + i, y);
			} else if (currentTileType == type) {
				// We found our matching tile. Convert the tiles we found.
				for (int k = 0; k < j; k++) {
					board[foundEnemyTiles[k].x][foundEnemyTiles[k].y] = type;
				}
				totalChangedTiles += j;
				lineEnded = true;
			} else {
				// We either found an empty space, or something else. We don't
				// convert anything
				lineEnded = true;
			}
		}
		
		// Check upper line
		for (i = 1, j = 0, lineEnded = false; y - i >= 0 && !lineEnded; i++) {
			TileColor currentTileType = board[x][y - i];
			// Add opposing tiles as we check
			if (currentTileType == oppositeType) {
				foundEnemyTiles[j++] = new BoardPosition(x, y - i);
			} else if (currentTileType == type) {
				// We found our matching tile. Convert the tiles we found.
				for (int k = 0; k < j; k++) {
					board[foundEnemyTiles[k].x][foundEnemyTiles[k].y] = type;
				}
				totalChangedTiles += j;
				lineEnded = true;
			} else {
				// We either found an empty space, or something else. We don't
				// convert anything
				lineEnded = true;
			}
		}
		
		// Check lower line
		for (i = 1, j = 0, lineEnded = false; y + i < BOARD_HEIGHT && !lineEnded; i++) {
			TileColor currentTileType = board[x][y + i];
			// Add opposing tiles as we check
			if (currentTileType == oppositeType) {
				foundEnemyTiles[j++] = new BoardPosition(x, y + i);
			} else if (currentTileType == type) {
				// We found our matching tile. Convert the tiles we found.
				for (int k = 0; k < j; k++) {
					board[foundEnemyTiles[k].x][foundEnemyTiles[k].y] = type;
				}
				totalChangedTiles += j;
				lineEnded = true;
			} else {
				// We either found an empty space, or something else. We don't
				// convert anything
				lineEnded = true;
			}
		}
		
		// Check upper-left line
		for (i = 1, j = 0, lineEnded = false; y - i >= 0 && x - i >= 0 && !lineEnded; i++) {
			TileColor currentTileType = board[x - i][y - i];
			// Add opposing tiles as we check
			if (currentTileType == oppositeType) {
				foundEnemyTiles[j++] = new BoardPosition(x - i, y - i);
			} else if (currentTileType == type) {
				// We found our matching tile. Convert the tiles we found.
				for (int k = 0; k < j; k++) {
					board[foundEnemyTiles[k].x][foundEnemyTiles[k].y] = type;
				}
				totalChangedTiles += j;
				lineEnded = true;
			} else {
				// We either found an empty space, or something else. We don't
				// convert anything
				lineEnded = true;
			}
		}
		
		// Check upper-right line
		for (i = 1, j = 0, lineEnded = false; y - i >= 0 && x + i < BOARD_WIDTH && !lineEnded; i++) {
			TileColor currentTileType = board[x + i][y - i];
			// Add opposing tiles as we check
			if (currentTileType == oppositeType) {
				foundEnemyTiles[j++] = new BoardPosition(x + i, y - i);
			} else if (currentTileType == type) {
				// We found our matching tile. Convert the tiles we found.
				for (int k = 0; k < j; k++) {
					board[foundEnemyTiles[k].x][foundEnemyTiles[k].y] = type;
				}
				totalChangedTiles += j;
				lineEnded = true;
			} else {
				// We either found an empty space, or something else. We don't
				// convert anything
				lineEnded = true;
			}
		}
		
		// Check lower-left line
		for (i = 1, j = 0, lineEnded = false; y + i < BOARD_HEIGHT && x - i >= 0 && !lineEnded; i++) {
			TileColor currentTileType = board[x - i][y + i];
			// Add opposing tiles as we check
			if (currentTileType == oppositeType) {
				foundEnemyTiles[j++] = new BoardPosition(x - i, y + i);
			} else if (currentTileType == type) {
				// We found our matching tile. Convert the tiles we found.
				for (int k = 0; k < j; k++) {
					board[foundEnemyTiles[k].x][foundEnemyTiles[k].y] = type;
				}
				totalChangedTiles += j;
				lineEnded = true;
			} else {
				// We either found an empty space, or something else. We don't
				// convert anything
				lineEnded = true;
			}
		}
		
		// Check lower-right line
		for (i = 1, j = 0, lineEnded = false; y + i < BOARD_HEIGHT && x + i < BOARD_WIDTH && !lineEnded; i++) {
			TileColor currentTileType = board[x + i][y + i];
			// Add opposing tiles as we check
			if (currentTileType == oppositeType) {
				foundEnemyTiles[j++] = new BoardPosition(x + i, y + i);
			} else if (currentTileType == type) {
				// We found our matching tile. Convert the tiles we found.
				for (int k = 0; k < j; k++) {
					board[foundEnemyTiles[k].x][foundEnemyTiles[k].y] = type;
				}
				totalChangedTiles += j;
				lineEnded = true;
			} else {
				// We either found an empty space, or something else. We don't
				// convert anything
				lineEnded = true;
			}
		}
		
		if (totalChangedTiles == 0) {
			throw new IllegalMoveException();
		}
		
		board[x][y] = type;
		
		if (type == TileColor.WHITE) {
			whiteTiles += totalChangedTiles + 1;
			blackTiles -= totalChangedTiles;
		}
		else {
			blackTiles += totalChangedTiles + 1;
			whiteTiles -= totalChangedTiles;
		}
		
		possibleWhiteMoves = null;
		possibleBlackMoves = null;
		
		return totalChangedTiles;
	}
	
	/**
	 * Checks if a tile with the given color can be placed in the given position
	 */
	public boolean isValidMove(TileColor type, BoardPosition position) {
		int x = position.x;
		int y = position.y;
		int i, j;
		TileColor oppositeType;
		
		if(board[x][y] != null){
            return false;
        }
		
		if (type == TileColor.WHITE) {
			oppositeType = TileColor.BLACK;
		}
		else if (type == TileColor.BLACK) {
			oppositeType = TileColor.WHITE;
		}
		else {
			return false;
		}
        
        // Check left line
		for (i = 1, j = 0; x - i >= 0; i++) {
			if (board[x - i][y] != oppositeType) {
				if (board[x - i][y] == type && j > 0) {
					return true;
				}
				break;
			}
			j++;
		}
        
        // Check right line
		for (i = 1, j = 0; x + i < BOARD_WIDTH; i++) {
			if (board[x + i][y] != oppositeType) {
				if (board[x + i][y] == type && j > 0) {
					return true;
				}
				break;
			}
			j++;
		}
        
        // Check upper line
		for (i = 1, j = 0; y - i >= 0; i++) {
			if (board[x][y - i] != oppositeType) {
				if (board[x][y - i] == type && j > 0) {
					return true;
				}
				break;
			}
			j++;
		}

        // Check lower line
		for (i = 1, j = 0; y + i < BOARD_HEIGHT; i++) {
			if (board[x][y + i] != oppositeType) {
				if (board[x][y + i] == type && j > 0) {
					return true;
				}
				break;
			}
			j++;
		}
        
        // Check upper-left line
		for (i = 1, j = 0; y - i >= 0 && x - i >= 0; i++) {
			if (board[x - i][y - i] != oppositeType) {
				if (board[x - i][y - i] == type && j > 0) {
					return true;
				}
				break;
			}
			j++;
		}
        
        // Check upper-right line
		for (i = 1, j = 0; y - i >= 0 && x + i < BOARD_WIDTH; i++) {
			if (board[x + i][y - i] != oppositeType) {
				if (board[x + i][y - i] == type && j > 0) {
					return true;
				}
				break;
			}
			j++;
		}
        
        // Check lower-left line
		for (i = 1, j = 0; y + i < BOARD_HEIGHT && x - i >= 0; i++) {
			if (board[x - i][y + i] != oppositeType) {
				if (board[x - i][y + i] == type && j > 0) {
					return true;
				}
				break;
			}
			j++;
		}

        // Check lower-right line
		for (i = 1, j = 0; y + i < BOARD_HEIGHT && x + i < BOARD_WIDTH; i++) {
			if (board[x + i][y + i] != oppositeType) {
				if (board[x + i][y + i] == type && j > 0) {
					return true;
				}
				break;
			}
			j++;
		}
		
        return false;
	}
	
	/**
	 * Returns the possible moves for the given color
	 */
	public List<BoardPosition> getPossibleMoves(TileColor type) {
		List<BoardPosition> list = new ArrayList<BoardPosition>();
		
		if (type == TileColor.WHITE) {
			if (possibleWhiteMoves != null)
				return possibleWhiteMoves;
		}
		else if (type == TileColor.BLACK ) {
			if (possibleBlackMoves != null)
				return possibleBlackMoves;
		}
		else {
			return list;
		}
		
		for (int i = 0; i < BOARD_HEIGHT; i++) {
			for (int j = 0; j < BOARD_WIDTH; j++) {
				BoardPosition currentPosition = new BoardPosition(j, i);
				if (isValidMove(type, currentPosition)) {
					list.add(currentPosition);
				}
			}
		}
		
		if (type == TileColor.WHITE) {
			possibleWhiteMoves = list;
		}
		else if (type == TileColor.BLACK ) {
			possibleBlackMoves = list;
		}
		
		return list;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < BOARD_HEIGHT; i++) {
			for (int j = 0; j < BOARD_WIDTH; j++) {
				TileColor color = board[j][i];
				
				if (color == TileColor.WHITE) {
					sb.append("W");
				}
				else if (color == TileColor.BLACK) {
					sb.append("B");
				}
				else {
					sb.append("-");
				}
			}
			sb.append("\n");
		}
		
		return sb.toString();
	}
	
	/**
	 * Returns the amount of tiles for the given color
	 */
	public int getTiles(TileColor type) {
		if (type == TileColor.WHITE) {
			return getWhiteTiles();
		}
		else if (type == TileColor.BLACK) {
			return getBlackTiles();
		}
		
		return 0;
	}
	
	public int getWhiteTiles() {
		return this.whiteTiles;
	}
	
	public int getBlackTiles() {
		return this.blackTiles;
	}
	
	/**
	 * Computes how many tiles each player has placed.
	 */
	private void computeTilesAmount() {
		whiteTiles = 0;
		blackTiles = 0;
		for (int i = 0; i < BOARD_HEIGHT; i++) {
			for (int j = 0; j < BOARD_WIDTH; j++) {
				TileColor color = board[j][i];
				
				if (color == TileColor.WHITE) {
					whiteTiles++;
				}
				else if (color == TileColor.BLACK) {
					blackTiles++;
				}
			}
		}
	}
	
	/**
	 * Returns the amount of empty tiles left in the board
	 */
	public int getEmptyTiles() {
		return BOARD_WIDTH * BOARD_HEIGHT - (getWhiteTiles() + getBlackTiles());
	}
	
	public TileColor getTileColor(int x, int y) {
		return board[x][y];
	}

}
