package com.gdelgiud.jversi;

import com.gdelgiud.jversi.ai.AIConfig;
import com.gdelgiud.jversi.ai.Minimax;
import com.gdelgiud.jversi.exception.InvalidMoveException;
import com.gdelgiud.jversi.exception.WrongPlayerTurnException;


public class ReversiApp {
	
	public static boolean gameEnded = false;

	public static void main(String[] args) {		
		ReversiGame game = new ReversiGame();

		AIConfig aiConfig = new AIConfig();
		aiConfig.setDepthLimited(3);
		
		System.out.println(game.getBoard());

		Minimax minimax = new Minimax();
		Turn result = null;
		
		try {
			while(!gameEnded) {
				result = minimax.runAI(game.getCurrentColor(), aiConfig, game.getBoard());
				
				switch (result.getType()) {
				case PASS:
					game.switchTurn();
					break;
				case PLAY:
					game.playTurn(game.getCurrentColor(), result.getMove());
					break;
				case RESIGN:
					gameEnded = true;
					break;
				}
				
				if (game.isFinished()) {
					gameEnded = true;
				}
				
				System.out.println(game.getBoard());
			}
		} catch (InvalidMoveException e) {
			System.err.println(e.getMessage());
		} catch (WrongPlayerTurnException e) {
			System.err.println(e.getMessage());
		}
	}

}
