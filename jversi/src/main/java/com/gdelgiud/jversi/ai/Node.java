package com.gdelgiud.jversi.ai;

import com.gdelgiud.jversi.Board;
import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.TileColor;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public abstract class Node {

	static int highest_id = 0;
	protected int id;

	protected Board board;
	public BoardPosition move;
	protected Node best;
	protected int value;
	protected int depth;

	public static long current_time;
	public static long end_time;

	protected static boolean collision_check;
	protected boolean cutoff = false; // True if Alpha-Beta prune deleted this node
	
	protected final AIConfig config;
	protected final TileColor playerColor;
	protected final TileColor enemyColor;

	public List<Node> children = new LinkedList<Node>();
	public static HashSet<Board> checked_boards;

	public Node(TileColor playerColor, Board board, BoardPosition move, int depth, final AIConfig config) {
		id = highest_id++;
		this.board = board;
		this.move = move;
		this.config = config;
		
		this.playerColor = playerColor;
		if (playerColor == TileColor.WHITE) {
			enemyColor = TileColor.BLACK;
		}
		else {
			enemyColor = TileColor.WHITE;
		}
	}

	/*
	 * @returns Whichever node is deemed better by the algorithm's current
	 * depth.
	 */
	public abstract Node chooseValue(Node a, Node b);

	/*
	 * Creates children for this node, each with an unique possible move
	 */
	public abstract void generateMoves();

	/*
	 * Applies the heuristic function to this node.
	 */
	public int heuristic() {
		// If the game ends here...
		if (board.getPossibleMoves(playerColor).isEmpty()
				&& board.getPossibleMoves(enemyColor).isEmpty()) {
			if (board.getTiles(playerColor) > board.getTiles(enemyColor)) {
				return 99999;
			} else if (board.getTiles(playerColor) <= board.getTiles(enemyColor)) {
				return -99999;
			}
		}

		// Compute the total stability for each player
		// Stability is the amount of tiles each player has
		// taking into account the positions they control.
		// For example, controlling the corners is more valuable
		// than controlling the center.
		int playerStability = 0, enemyStability = 0;
		for (int i = 0; i < Board.BOARD_HEIGHT; i++) {
			for (int j = 0; j < Board.BOARD_WIDTH; j++) {
				TileColor currentTile = board.getTileColor(j, i);
				int precalc = Minimax.precalculatedValues[j][i];
				
				if (currentTile == playerColor) {
					playerStability += precalc;
				} else if (currentTile == enemyColor) {
					enemyStability += precalc;
				}
			}
		}
		
		List<BoardPosition> playerMoves = board.getPossibleMoves(playerColor);
		List<BoardPosition> enemyMoves = board.getPossibleMoves(enemyColor);
		
		int playerTiles = board.getTiles(playerColor);
		int enemyTiles = board.getTiles(enemyColor);

		if (board.getEmptyTiles() > 30) {
			return (3 * (playerStability - enemyStability) + 3
					* (playerMoves.size() - enemyMoves.size())
					- 20 * (playerTiles - enemyTiles));
		} else if (board.getEmptyTiles() > 12) {
			return (2 * (playerStability - enemyStability) + 3
					* (playerMoves.size() - enemyMoves.size())
					- (playerTiles - enemyTiles));
		} else {
			return ((playerStability - enemyStability) / 2
					+ (playerMoves.size() - enemyMoves.size())
					+ 2 * (playerTiles - enemyTiles));
		}
	}

	/*
	 * @returns true if the new node must be processed
	 * 
	 * @returns false if the new node is worthless
	 */
	public abstract boolean applyPruning(Node prev, Node curr);

	/*
	 * Recursively apply the Minimax Algorithm
	 * 
	 * @param depth The current depth
	 * 
	 * @param max_depth The last explored depth
	 * 
	 * @param best_value The value chosen by the previous sibling, used by
	 * Alpha-Beta pruning
	 * 
	 * @returns True if the algorithm ended normally
	 * @returns False if the algorithm timed out
	 */
	private boolean minimax(int depth, int maxDepth, long startTime, Node best_value) {
		if (config.isTimeLimited() && System.currentTimeMillis() > (startTime + config.getTimeLimit())) {
			return false;
		}
		if (depth == maxDepth) {
			value = heuristic();
		} else {
			boolean pruned = false;
			generateMoves();
			for (Node n : children) {
				if (!pruned) {
					boolean timedOut = !n.minimax(depth + 1, maxDepth, startTime, best);
					if (timedOut) {
						return false;
					}
					
					best = chooseValue(best, n);

					if (config.isPruning() && !applyPruning(best_value, n)) {
						pruned = true;
					}
				} else {
					n.cutoff = true;
				}
			}
			if (best != null)
				value = best.value;
		}
		board = null;
		
		return true;
	}
	
	/*
	 * Recursively apply the Minimax Algorithm
	 * @returns True if the algorithm ended normally
	 * @returns False if the algorithm timed out
	 */
	public boolean minimax(int maxDepth, long startTime) {
		return minimax(0, maxDepth, startTime, null);
	}

	/*
	 * Save the tree's definition in a DOT file.
	 */
	public void saveDot(String filename) {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(filename));
			out.println("graph{");
			this.saveDot(out);
			out.println("}");
			out.close();
		} catch (Exception e) {
			System.out.println("Error saving DOT file: " + e.getMessage());
		}
	}

	/*
	 * Sets the time limit for the algorithm.
	 */
	public static void setTimeLimit(int time_limit) {
		end_time = System.currentTimeMillis() + time_limit * 1000;
	}

	protected final void writeNode(PrintWriter out, String shape) {
		out.printf("%d [shape=\"%s\"];\n", id, shape);
		if (move == null) {
			out.printf("%d [label=\"%s %d\"];\n", id, "START", value);
		} else {
			if (cutoff) {
				out.printf("%d [label=\"%s\"];\n", id, move.toString());
				out.printf("%d [style=filled, fillcolor=%s];\n", id, "grey77");
				return;
			}
			out.printf("%d [label=\"%s %d\"];\n", id, move.toString(), value);

		}
		for (Node n : children) {
			n.saveDot(out);
			out.printf("%d -- %d;\n", id, n.id);
			if (n == best) {
				out.printf("%d [style=filled, fillcolor=%s];\n", n.id, "tomato");
			}
		}
	}

	protected abstract void saveDot(PrintWriter out);

}
