package com.gdelgiud.jversi.ai;

import com.gdelgiud.jversi.Board;
import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.jversi.Turn;
import com.gdelgiud.jversi.Turn.TurnType;

public class Minimax {

	private MaxNode root;
	private long timeTaken;
	
	public static int precalculatedValues[][] = {
		{75,-1,1,1,1,1,-1,75},
    	{-1,-5,2,2,2,2,-5,-1},
        {1,2,4,4,4,4,2,1},
        {1,2,4,5,5,4,2,1},
        {1,2,4,5,5,4,2,1},
        {1,2,4,4,4,4,2,1},
        {-1,-5,2,2,2,2,-5,-1},
        {75,-1,1,1,1,1,-1,75}};

	public Turn runAI(final TileColor color, final AIConfig aiConfig, final Board board) {
		long start = 0;
		BoardPosition move = null;
		Turn result = new Turn(color, TurnType.PASS);
		
		if (aiConfig.isTimeLimited()) {
			timeLimitedSearch(color, aiConfig, board);
		} else {
			depthLimitedSearch(color, aiConfig, board);
		}

		timeTaken = System.currentTimeMillis() - start;

		move = bestMove();
		
		if (move != null) {
			result = new Turn(color, TurnType.PLAY, move);
		}
		
		return result;
	}

	private void depthLimitedSearch(final TileColor color, final AIConfig aiConfig, final Board board) {
		Board currentBoard = new Board(board);
		root = new MaxNode(color, currentBoard, null, 0, aiConfig);
		root.minimax(aiConfig.getMaxDepth(), 0);
	}

	private void timeLimitedSearch(final TileColor color, final AIConfig aiConfig, final Board board) {
		long start = 0;
		MaxNode tmp_root;
		Board currentBoard = new Board(board);
		
		int depth = 2;
		start = System.currentTimeMillis();
		boolean timedOut = false;
		while (!timedOut) {
			// Perform the search in a temporary node, and copy it when
			// finished.
			tmp_root = new MaxNode(color, currentBoard, null, 0, aiConfig);
			timedOut = !tmp_root.minimax(depth, start);
			depth++;
			
			if (!timedOut)
				root = tmp_root;
		}
		timeTaken = System.currentTimeMillis() - start;
	}

	/*
	 * Returns the time in milliseconds taken to choose the best move.
	 */
	public long getTimeTaken() {
		return timeTaken;
	}

	/*
	 * Returns the best move chosen by the algorithm.
	 */
	public BoardPosition bestMove() {
		if (root.best == null)
			return null;
		return root.best.move;
	}

	/*
	 * Saves this tree into a DOT file.
	 */
	public void saveDot(String filename) {
		root.saveDot(filename);
	}
}
