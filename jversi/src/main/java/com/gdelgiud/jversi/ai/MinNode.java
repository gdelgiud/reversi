package com.gdelgiud.jversi.ai;

import com.gdelgiud.jversi.Board;
import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.jversi.exception.InvalidMoveException;

import java.io.PrintWriter;

public class MinNode extends Node {

	MinNode(TileColor playerColor, Board board, BoardPosition move, int depth, final AIConfig config) {
		super(playerColor, board, move, depth, config);
		if (move != null) {
			try {
				board.playTile(playerColor, move);
			} catch (InvalidMoveException ex) {
				// The AI should have never tried to play an illegal move
				System.err.println("AI error: tried to play an illegal move in a MIN node.");
				System.err.println(board);
				System.err.println("Position: " + move);
				System.err.println("Depth: " + depth);
			}
		}
	}

	@Override
	public Node chooseValue(Node a, Node b) {
		if (a == null) {
			return b;
		} else if (b == null) {
			return a;
		}

		return (a.value < b.value) ? a : b;
	}

	@Override
	public void generateMoves() {
		for (BoardPosition v : board.getPossibleMoves(enemyColor)) {
			// If the board wasn't evaluated before, do so.
			Board copy = new Board(board);
			// if(collision_check){
			// if(checked_boards.add(copy) == true)
			children.add(new MaxNode(playerColor, copy, v, depth + 1, config));
			/*
			 * } else{ children.add(new Max(player, copy, v, depth + 1)); }
			 */
		}
	}

	@Override
	protected void saveDot(PrintWriter out) {
		writeNode(out, "oval");
	}

	@Override
	public boolean applyPruning(Node prev, Node curr) {
		if (prev == null) {
			return true;
		}

		if (curr.value <= prev.value) {
			return false;
		}

		return true;
	}

}
