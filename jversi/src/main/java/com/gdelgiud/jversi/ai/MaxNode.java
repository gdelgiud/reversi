package com.gdelgiud.jversi.ai;

import com.gdelgiud.jversi.Board;
import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.jversi.exception.InvalidMoveException;

import java.io.PrintWriter;

public class MaxNode extends Node {

	MaxNode(TileColor playerColor, Board board, BoardPosition move, int depth, final AIConfig config) {
		super(playerColor, board, move, depth, config);
		if (move != null) {
			try {
				board.playTile(enemyColor, move);
			} catch (InvalidMoveException ex) {
				// The AI should have never tried to play an illegal move
				System.err.println("AI error: tried to play an illegal move in a MAX node.");
				System.err.println(board);
				System.err.println("Position: " + move);
				System.err.println("Depth: " + depth);
			}
		}
	}

	@Override
	public Node chooseValue(Node a, Node b) {
		if (a == null) {
			return b;
		} else if (b == null) {
			return a;
		}

		return (a.value > b.value) ? a : b;
	}

	@Override
	public void generateMoves() {
		for (BoardPosition v : board.getPossibleMoves(playerColor)) {
			// If the board wasn't evaluated before, do so.
			Board copy = new Board(board);
			// if(collision_check){
			// if(checked_boards.add(copy) == true)
			children.add(new MinNode(playerColor, copy, v, depth + 1, config));
			/*
			 * } else{ children.add(new Min(player, copy, v, depth + 1)); }
			 */
		}
	}

	@Override
	protected void saveDot(PrintWriter out) {
		writeNode(out, "box");
	}

	@Override
	public boolean applyPruning(Node prev, Node curr) {
		if (prev == null) {
			return true;
		}

		if (curr.value >= prev.value) {
			return false;
		}

		return true;
	}

}
