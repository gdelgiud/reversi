package com.gdelgiud.jversi.ai;

import java.io.Serializable;

public class AIConfig implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private boolean timeLimited;
	private long timeLimit;
	private int maxDepth;
	
	private final boolean pruning;
	
	/**
	 * Creates a default AIConfig. This AIConfig is depth-limited, with maximum depth of 3 levels, and alpha-beta pruning enabled.
	 */
	public AIConfig() {
		timeLimited = false;
		timeLimit = 0;
		maxDepth = 3;
		pruning = true;
	}
	
	/**
	 * Sets the AI to run up to the given depth.
	 * @param maxDepth The depth limit. Must be a positive number.
	 */
	public void setDepthLimited(int maxDepth) {
		if (maxDepth <= 0) {
			throw new IllegalArgumentException("Depth limit must be a positive number");
		}
		
		this.maxDepth = maxDepth;
		timeLimited = false;
	}
	
	/**
	 * Sets the AI to run roughly for the given time in milliseconds. 
	 * @param timeLimit The time limit in milliseconds. Must be a positive number.
	 */
	public void setTimeLimited(long timeLimit) {
		if (timeLimit <= 0) {
			throw new IllegalArgumentException("Time limit must be a positive number");
		}
		
		this.timeLimit = timeLimit;
		timeLimited = true;
	}
	
	public boolean isTimeLimited() {
		return timeLimited;
	}
	
	public long getTimeLimit() {
		return timeLimit;
	}
	
	public int getMaxDepth() {
		return maxDepth;
	}
	
	public boolean isPruning() {
		return pruning;
	}

}
