package com.gdelgiud.jversi;

import com.gdelgiud.jversi.Turn.TurnType;
import com.gdelgiud.jversi.exception.InvalidMoveException;
import com.gdelgiud.jversi.exception.WrongPlayerTurnException;

import java.io.Serializable;

public class ReversiGame implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private TileColor currentColor;
	private Board board;
	private Turn lastTurn;
	
	public ReversiGame() {
		reset();
	}
	
	public void reset() {
		currentColor = TileColor.WHITE;
		lastTurn = null;
		
		if (board == null) {
			board = new Board();
		}
		
		board.reset();
	}
	
	public boolean isFinished() {
		return board.getEmptyTiles() <= 0 || (board.getPossibleMoves(TileColor.WHITE).isEmpty() && board.getPossibleMoves(TileColor.BLACK).isEmpty())
				|| (lastTurn != null && lastTurn.getType() == TurnType.RESIGN);
	}
	
	public TileColor getWinningPlayer() {
		TileColor winner;
		
		if ((lastTurn != null && lastTurn.getType() == TurnType.RESIGN && lastTurn.getColor() == TileColor.BLACK) ||
				board.getWhiteTiles() > board.getBlackTiles()) {
			winner = TileColor.WHITE;
		}
		else if ((lastTurn != null && lastTurn.getType() == TurnType.RESIGN && lastTurn.getColor() == TileColor.WHITE) ||
				board.getWhiteTiles() < board.getBlackTiles()) {
			winner = TileColor.BLACK;
		}
		else {
			winner = null;
		}
		
		return winner;
	}
	
	public void playTurn(TileColor color, BoardPosition position) throws InvalidMoveException, WrongPlayerTurnException {
		if (color == currentColor) {
			board.playTile(color, position);
			switchTurn();
			lastTurn = new Turn(color, TurnType.PLAY, position);
		}
		else {
			throw new WrongPlayerTurnException();
		}
	}
	
	public void pass(TileColor color) {
		if (color == currentColor) {
			lastTurn = new Turn(color, TurnType.PASS);
			switchTurn();
		}
	}
	
	public void resign(TileColor color) {
		lastTurn = new Turn(color, TurnType.RESIGN);
	}
	
	public void switchTurn() {
		currentColor = currentColor.opposing();
	}
	
	public TileColor getCurrentColor() {
		return currentColor;
	}
	
	public Board getBoard() {
		return board;
	}
	
	public Turn getLastTurn() {
		return lastTurn;
	}
}
