package com.gdelgiud.jversi;

import java.io.Serializable;

public class Turn implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum TurnType {
		PLAY,
		PASS,
		RESIGN
	}
	
	private final BoardPosition move;
	private final TurnType type;
	private final TileColor color;
	
	public Turn(TileColor color, TurnType type, BoardPosition move) {
		this.type = type;
		this.move = move;
		this.color = color;
	}
	
	public Turn(TileColor color, TurnType type) {
		this(color, type, null);
	}
	
	public BoardPosition getMove() {
		return move;
	}
	
	public TurnType getType() {
		return type;
	}
	
	public TileColor getColor() {
		return color;
	}
}
