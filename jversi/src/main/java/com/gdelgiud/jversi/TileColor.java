package com.gdelgiud.jversi;

public enum TileColor {
	WHITE,
	BLACK;
	
	public TileColor opposing() {
		if (equals(WHITE)) {
			return BLACK;
		}
		else {
			return WHITE;
		}
	}
}
