package com.gdelgiud.jversi;

import java.io.Serializable;

public class BoardPosition implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public final int x;
	public final int y;
	
	public BoardPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
    public String toString(){
        return "(" + x + ", " + y + ")";
    }
}
