package com.gdelgiud.reversi.domain;

import com.gdelgiud.reversi.model.Difficulty;

public interface LastValuesRepository {
    String getLastPlayer1Name();
    String getLastPlayer2Name();
    void setLastPlayer1Name(String name);
    void setLastPlayer2Name(String name);
    void clearLastPlayer1Name();
    void clearLastPlayer2Name();
    Difficulty getLastPlayer1Difficulty();
    Difficulty getLastPlayer2Difficulty();
    void setLastPlayer1Difficulty(Difficulty difficulty);
    void setLastPlayer2Difficulty(Difficulty difficulty);
}
