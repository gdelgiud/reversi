package com.gdelgiud.reversi.domain;

import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.Turn;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface GameHandler {

    /**
     * Returns the current {@link Game}
     *
     * @return The current {@link Game}
     */
    Game getGame();

    /**
     * Sets the given {@link Game} as the current one and emits it to all subscribers.
     *
     * @param game The game to set as current
     */
    void setGame(Game game);

    /**
     * Creates a new {@link Game} using the same players as the current one. The new {@link Game}
     * is then saved.
     *
     * @return The new {@link Game}
     */
    Game newGame();

    /**
     * Persists the current {@link Game}
     */
    void saveGame();

    /**
     * Removes the current {@link Game} from storage.
     */
    void removeGame();

    /**
     * Returns an {@link Observable} that notifies about any change in the current {@link Game}'s
     * status. Emits the current status on subscription.
     *
     * @return An {@link Observable} for the current {@link Game}
     */
    Observable<Game> getGameObservable();

    /**
     * Returns whether the current player can be controlled (is human) or not (is computer-controlled)
     *
     * @return True if the player is controllable, False otherwise
     */
    boolean isPlayerControllable();

    /**
     * Returns whether the given {@link BoardPosition} can be played by the current player.
     *
     * @return True if the given position is valid, False otherwise
     */
    boolean isValidMove(BoardPosition position);

    /**
     * Makes the current player make their move in the given {@link BoardPosition}.
     *
     * @param position The position to play the tile at
     */
    void playTile(BoardPosition position);

    /**
     * Processes the logic needed for the next turn. If the current player is computer-controlled,
     * runs its AI.
     */
    void processTurn();

    /**
     * Returns all the available moves for the current player. Returns an empty list if the player
     * has no available moves.
     *
     * @return The available moves for the current player
     */
    List<BoardPosition> getAvailableMoves();

    /**
     * Uses a weaker AI to get a hint for the current player.
     *
     * @return A {@link Single} that emits the chosen {@link Turn} for the current player
     */
    Single<Turn> hint();

    /**
     * Makes the current player pass their turn.
     */
    void passTurn();

    /**
     * Makes the current player forfeit the game.
     */
    void resign();

    /**
     * Returns which player has won the {@link Game}
     * @return The player that won the {@link Game}
     */
    Player getWinningPlayer();
}
