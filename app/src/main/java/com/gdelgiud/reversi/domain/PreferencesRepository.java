package com.gdelgiud.reversi.domain;


public interface PreferencesRepository {
    void setShowAvailableMovesEnabled(boolean enabled);
    boolean isShowAvailableMovesEnabled();
}
