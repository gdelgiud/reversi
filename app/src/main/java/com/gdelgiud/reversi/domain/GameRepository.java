package com.gdelgiud.reversi.domain;


import android.support.annotation.NonNull;

import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;

import java.util.List;

public interface GameRepository {

    /**
     * Creates a new game with the given Players.
     * @return The resulting Game
     */
    Game createGame(Player whitePlayer, Player blackPlayer);

    /**
     * Adds a new Game. A Game ID is automatically assigned.
     * @param game The game to add
     */
    void addGame(@NonNull final Game game);

    /**
     * Finds a Game with the given Game's ID, and replaces it. Does nothing if no such Game exists.
     * @param game The Game to update
     * @return True if the Game was updated, False otherwise
     */
    boolean updateGame(@NonNull final Game game);

    /**
     * Finds a Game with the given Game's ID, and removes it.
     * @param game The game to remove
     * @return True if the Game was removed, False otherwise
     */
    boolean removeGame(@NonNull final Game game);

    /**
     * Removes all saved games.
     */
    void removeAllGames();

    /**
     * Reads through the saved games list and places them in the managed list.
     */
    void loadGames();

    /**
     * Returns a copy of the Game List. Since this can be an expensive operation, do not call this
     * method repeatedly.
     * @return A copy of the Game List.
     */
    List<Game> getGames();

    /**
     * Returns whether there are saved games or not
     */
    boolean hasGames();

}
