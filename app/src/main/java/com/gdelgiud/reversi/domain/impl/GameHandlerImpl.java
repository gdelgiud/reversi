package com.gdelgiud.reversi.domain.impl;


import android.support.annotation.VisibleForTesting;

import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.Turn;
import com.gdelgiud.jversi.ai.AIConfig;
import com.gdelgiud.jversi.ai.Minimax;
import com.gdelgiud.jversi.exception.InvalidMoveException;
import com.gdelgiud.jversi.exception.WrongPlayerTurnException;
import com.gdelgiud.reversi.domain.GameHandler;
import com.gdelgiud.reversi.domain.GameRepository;
import com.gdelgiud.reversi.injection.scope.ActivityScoped;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.subjects.BehaviorSubject;

@ActivityScoped
public class GameHandlerImpl implements GameHandler {

    private final GameRepository gameRepository;
    private final Minimax minimax;
    private final AIConfig hintAiConfig;
    private final Scheduler computationScheduler;
    private final Scheduler mainThreadScheduler;
    private final BehaviorSubject<Game> gameSubject;

    private Game game;
    private Turn cachedHint;

    @Inject
    public GameHandlerImpl(GameRepository gameRepository,
                           Minimax minimax,
                           AIConfig hintAiConfig,
                           @Named("computation") Scheduler computationScheduler,
                           @Named("mainThread") Scheduler mainThreadScheduler) {
        this.computationScheduler = computationScheduler;
        this.mainThreadScheduler = mainThreadScheduler;
        this.gameRepository = gameRepository;
        this.minimax = minimax;
        this.hintAiConfig = hintAiConfig;

        gameSubject = BehaviorSubject.create();
    }

    @Override
    public Game getGame() {
        return game;
    }

    @Override
    public void setGame(Game game) {
        this.game = game;
        gameSubject.onNext(game);
    }

    @Override
    public Observable<Game> getGameObservable() {
        return gameSubject;
    }

    @Override
    public void saveGame() {
        gameRepository.updateGame(game);
    }

    @Override
    public Game newGame() {
        final Player whitePlayer = game.getWhitePlayer();
        final Player blackPlayer = game.getBlackPlayer();
        final Game newGame = gameRepository.createGame(whitePlayer, blackPlayer);

        setGame(newGame);
        return newGame;
    }

    @Override
    public void removeGame() {
        gameRepository.removeGame(game);
    }

    @Override
    public void playTile(BoardPosition boardPosition) {
        try {
            game.playTurn(game.getCurrentPlayer().getColor(), boardPosition);
            gameSubject.onNext(game);
            cachedHint = null;
        } catch (InvalidMoveException | WrongPlayerTurnException e) {
            gameSubject.onError(e);
        }
    }

    @Override
    public void passTurn() {
        game.pass(game.getCurrentPlayer().getColor());
        gameSubject.onNext(game);
    }

    @Override
    public void resign() {
        game.resign(game.getCurrentPlayer().getColor());
        gameSubject.onNext(game);
    }

    @Override
    public Single<Turn> hint() {
        Single<Turn> ret;
        if (cachedHint == null) {
            ret = Single.fromCallable(new Callable<Turn>() {
                @Override
                public Turn call() throws Exception {
                    return getHint();
                }
            })
                    .subscribeOn(computationScheduler)
                    .observeOn(mainThreadScheduler);
        } else {
            ret = Single.just(cachedHint);
        }
        return ret;
    }

    @Override
    public boolean isValidMove(BoardPosition position) {
        return game.getBoard().isValidMove(game.getCurrentPlayer().getColor(), position);
    }

    @Override
    public boolean isPlayerControllable() {
        return game.getCurrentPlayer().isControllable();
    }

    @Override
    public List<BoardPosition> getAvailableMoves() {
        return game.getBoard().getPossibleMoves(game.getCurrentPlayer().getColor());
    }

    @Override
    public Player getWinningPlayer() {
        return game.getPlayer(game.getWinningPlayer());
    }

    @Override
    public void processTurn() {
        if (!game.getCurrentPlayer().isControllable()) {
            runAi().subscribe(new DisposableSingleObserver<Turn>() {
                @Override
                public void onSuccess(Turn turn) {
                    applyTurn(turn);
                }

                @Override
                public void onError(Throwable e) {
                    onTurnError(e);
                }
            });
        }
    }

    void applyTurn(Turn turn) {
        switch (turn.getType()) {
            case PLAY:
                playTile(turn.getMove());
                break;
            case PASS:
                passTurn();
                break;
            case RESIGN:
                resign();
                break;
        }
    }

    Turn getHint() {
        cachedHint = minimax.runAI(game.getCurrentPlayer().getColor(), hintAiConfig, game.getBoard());
        return cachedHint;
    }

    Turn getAiTurn() {
        return minimax.runAI(game.getCurrentPlayer().getColor(),
                game.getCurrentPlayer().getAiConfig(), game.getBoard());
    }

    void onTurnError(Throwable error) {
        gameSubject.onError(error);
    }

    @VisibleForTesting
    boolean hintIsCached() {
        return cachedHint != null;
    }

    private Single<Turn> runAi() {
        return Single.fromCallable(new Callable<Turn>() {
            @Override
            public Turn call() throws Exception {
                return getAiTurn();
            }
        }).subscribeOn(computationScheduler)
        .observeOn(mainThreadScheduler);
    }
}
