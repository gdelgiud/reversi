package com.gdelgiud.reversi.injection.module;

import android.app.Application;
import android.content.Context;

import com.gdelgiud.jversi.ai.AIConfig;
import com.gdelgiud.jversi.ai.Minimax;
import com.gdelgiud.reversi.data.GameRepositoryImpl;
import com.gdelgiud.reversi.data.LastValuesRepositoryImpl;
import com.gdelgiud.reversi.data.PreferencesRepositoryImpl;
import com.gdelgiud.reversi.domain.GameRepository;
import com.gdelgiud.reversi.domain.LastValuesRepository;
import com.gdelgiud.reversi.domain.PreferencesRepository;
import com.gdelgiud.reversi.injection.name.Computation;
import com.gdelgiud.reversi.injection.name.MainThread;
import com.gdelgiud.reversi.injection.scope.ApplicationContext;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    GameRepository provideGameManager(final Application application, final Gson gson) {
        return new GameRepositoryImpl(application, gson);
    }

    @Provides
    @Singleton
    LastValuesRepository provideLastValuesRepository(LastValuesRepositoryImpl lastValuesRepository) {
        return lastValuesRepository;
    }

    @Provides
    @Singleton
    PreferencesRepository providePreferencesRepository(PreferencesRepositoryImpl preferencesRepository) {
        return preferencesRepository;
    }

    @Provides
    @Singleton
    Minimax provideMinimax() {
        return new Minimax();
    }

    @Provides
    @Singleton
    AIConfig provideHintAiConfig() {
        final AIConfig hintAiConfig = new AIConfig();
        hintAiConfig.setDepthLimited(1);
        return hintAiConfig;
    }

    @Provides
    @Computation
    @Singleton
    Scheduler provideComputationScheduler() {
        return Schedulers.computation();
    }

    @Provides
    @MainThread
    @Singleton
    Scheduler provideMainThreadScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
