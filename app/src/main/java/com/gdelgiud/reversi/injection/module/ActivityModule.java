package com.gdelgiud.reversi.injection.module;

import android.app.Activity;
import android.content.Context;

import com.gdelgiud.jversi.ai.AIConfig;
import com.gdelgiud.jversi.ai.Minimax;
import com.gdelgiud.reversi.domain.GameHandler;
import com.gdelgiud.reversi.domain.GameRepository;
import com.gdelgiud.reversi.domain.impl.GameHandlerImpl;
import com.gdelgiud.reversi.injection.name.Computation;
import com.gdelgiud.reversi.injection.name.MainThread;
import com.gdelgiud.reversi.injection.scope.ActivityScoped;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

@Module
public class ActivityModule {

    private Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    Activity provideActivity() {
        return activity;
    }

    @Provides
    @ActivityScoped
    Context provideContext() {
        return activity;
    }

    @Provides
    @ActivityScoped
    GameHandler provideGameHandler(GameRepository gameManager, Minimax minimax, AIConfig hintAiConfig,
                                   @Computation Scheduler computationScheduler,
                                   @MainThread Scheduler mainThreadScheduler) {
        return new GameHandlerImpl(gameManager, minimax, hintAiConfig, computationScheduler,
                mainThreadScheduler);
    }

}
