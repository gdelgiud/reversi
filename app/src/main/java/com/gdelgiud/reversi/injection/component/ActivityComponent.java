package com.gdelgiud.reversi.injection.component;

import com.gdelgiud.reversi.ui.game.GameActivity;
import com.gdelgiud.reversi.ui.main.MainActivity;
import com.gdelgiud.reversi.ui.newgame.NewGameActivity;
import com.gdelgiud.reversi.ui.saves.SavedGamesActivity;
import com.gdelgiud.reversi.injection.module.ActivityModule;
import com.gdelgiud.reversi.injection.scope.ActivityScoped;

import dagger.Component;

@ActivityScoped
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(MainActivity activity);
    void inject(GameActivity activity);
    void inject(NewGameActivity activity);
    void inject(SavedGamesActivity activity);
}
