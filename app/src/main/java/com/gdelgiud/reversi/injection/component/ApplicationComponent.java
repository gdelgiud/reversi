package com.gdelgiud.reversi.injection.component;

import android.content.Context;

import com.gdelgiud.jversi.ai.AIConfig;
import com.gdelgiud.jversi.ai.Minimax;
import com.gdelgiud.reversi.domain.GameRepository;
import com.gdelgiud.reversi.domain.LastValuesRepository;
import com.gdelgiud.reversi.domain.PreferencesRepository;
import com.gdelgiud.reversi.injection.module.ApplicationModule;
import com.gdelgiud.reversi.injection.name.Computation;
import com.gdelgiud.reversi.injection.name.MainThread;
import com.gdelgiud.reversi.injection.scope.ApplicationContext;

import javax.inject.Singleton;

import dagger.Component;
import io.reactivex.Scheduler;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext Context context();
    GameRepository gameRepository();
    LastValuesRepository lastValuesRepository();
    PreferencesRepository preferencesRepository();
    Minimax minimax();
    AIConfig hintAiConfig();
    @Computation
    Scheduler computationScheduler();
    @MainThread
    Scheduler mainThreadScheduler();
}
