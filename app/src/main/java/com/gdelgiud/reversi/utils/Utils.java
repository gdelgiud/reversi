package com.gdelgiud.reversi.utils;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.StringRes;
import android.util.DisplayMetrics;
import android.widget.Toast;

public class Utils {

    public static void showShortToast(final Context context, final String message) {
        showToast(context, message, Toast.LENGTH_SHORT);
    }

    public static void showLongToast(final Context context, final String message) {
        showToast(context, message, Toast.LENGTH_LONG);
    }

    public static void showShortToast(final Context context, @StringRes final int resId) {
        showToast(context, resId, Toast.LENGTH_SHORT);
    }

    public static void showLongToast(final Context context, @StringRes final int resId) {
        showToast(context, resId, Toast.LENGTH_LONG);
    }

    public static void showToast(final Context context, @StringRes final int resId,
                                 final int duration) {
        Toast toast = Toast.makeText(context, resId, duration);
        toast.show();
    }

    public static void showToast(final Context context, final String message, final int duration) {
        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }

    public static float convertPixelsToDp(float px){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return Math.round(dp);
    }

    public static float convertDpToPixel(float dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }
}
