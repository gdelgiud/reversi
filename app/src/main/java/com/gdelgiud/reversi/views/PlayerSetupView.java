package com.gdelgiud.reversi.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.jversi.ai.AIConfig;
import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.model.Difficulty;
import com.gdelgiud.reversi.model.PlayerSetupModel;
import com.gdelgiud.reversi.model.PlayerType;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlayerSetupView extends RelativeLayout {

    private static final int HUMAN_INDEX = 0;
    private static final int COMPUTER_INDEX = 1;

    public interface Callback {
        void onColorSelected(TileColor color);
    }

    @BindView(R.id.title)
    protected TextView titleView;
    @BindView(R.id.name_edit)
    protected EditText nameEdit;
    @BindView(R.id.white_button)
    protected Button whiteButton;
    @BindView(R.id.black_button)
    protected Button blackButton;
    @BindView(R.id.easy_button)
    protected Button easyButton;
    @BindView(R.id.medium_button)
    protected Button mediumButton;
    @BindView(R.id.hard_button)
    protected Button hardButton;
    @BindView(R.id.player_type_view_switcher)
    protected ViewSwitcher playerTypeViewSwitcher;

    private Difficulty difficulty;
    private TileColor playerColor;
    private PlayerType playerType;
    private String randomName;

    private Callback callback;

    public PlayerSetupView(Context context) {
        super(context);
        init();
    }

    public PlayerSetupView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PlayerSetupView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PlayerSetupView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    protected void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.player_setup_layout, this, true);
        if (isInEditMode()) {
            return;
        }
        ButterKnife.bind(this);

        if (difficulty == null) {
            difficulty = Difficulty.MEDIUM;
        }
    }

    public PlayerSetupModel getPlayer() {
        return new PlayerSetupModel(getName(), getDifficulty(), getPlayerType(), getPlayerColor(),
                TextUtils.isEmpty(nameEdit.getText().toString()));
    }

    public void setTitle(final String title) {
        titleView.setText(title);
    }

    public void setTitle(@StringRes int resId) {
        titleView.setText(resId);
    }

    public void setName(final String name) {
        nameEdit.setText(name);
    }

    public String getName() {
        return getName(true);
    }

    public String getName(final boolean useRandom) {
        String name;
        if (TextUtils.isEmpty(nameEdit.getText().toString()) && useRandom) {
            name = randomName;
        }
        else {
            name = nameEdit.getText().toString();
        }
        return name;
    }

    public String getRandomName() {
        return randomName;
    }

    public void generateRandomName() {
        TypedArray array = getResources().obtainTypedArray(R.array.names);
        final int index = new Random().nextInt(array.length());
        randomName = array.getString(index);
        nameEdit.setHint(randomName);
        array.recycle();
    }

    public void setPlayerType(PlayerType type) {
        playerType = type;

        switch (type) {

            case HUMAN:
                playerTypeViewSwitcher.setDisplayedChild(HUMAN_INDEX);
                break;
            case COMPUTER:
                playerTypeViewSwitcher.setDisplayedChild(COMPUTER_INDEX);
                break;
        }
    }

    public PlayerType getPlayerType() {
        return playerType;
    }

    public void setComputerDifficulty(Difficulty difficulty) {
        easyButton.setEnabled(difficulty != Difficulty.EASY);
        mediumButton.setEnabled(difficulty != Difficulty.MEDIUM);
        hardButton.setEnabled(difficulty != Difficulty.HARD);

        this.difficulty = difficulty;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setPlayerColor(TileColor color) {
        final boolean isWhite = color == TileColor.WHITE;

        whiteButton.setEnabled(!isWhite);
        blackButton.setEnabled(isWhite);

        playerColor = color;
    }

    public TileColor getPlayerColor() {
        return playerColor;
    }

    public void setCallback(final Callback callback) {
        this.callback = callback;
    }

    @OnClick(R.id.white_button)
    public void selectWhite() {
        setPlayerColor(TileColor.WHITE);
        if (callback != null) {
            callback.onColorSelected(TileColor.WHITE);
        }
    }

    @OnClick(R.id.black_button)
    public void selectBlack() {
        setPlayerColor(TileColor.BLACK);
        if (callback != null) {
            callback.onColorSelected(TileColor.BLACK);
        }
    }

    @OnClick(R.id.easy_button)
    public void easyButtonClicked() {
        setComputerDifficulty(Difficulty.EASY);
    }

    @OnClick(R.id.medium_button)
    public void mediumButtonClicked() {
        setComputerDifficulty(Difficulty.MEDIUM);
    }

    @OnClick(R.id.hard_button)
    public void hardButtonClicked() {
        setComputerDifficulty(Difficulty.HARD);
    }

    public AIConfig getAiConfig() {
        AIConfig aiConfig = null;
        if (playerType == PlayerType.COMPUTER) {
            aiConfig = new AIConfig();
            int depth;

            switch (getDifficulty()) {
                case EASY:
                    depth = 3;
                    break;
                default:
                case MEDIUM:
                    depth = 4;
                    break;
                case HARD:
                    depth = 5;
                    break;
            }

            aiConfig.setDepthLimited(depth);
        }

        return aiConfig;
    }

}
