package com.gdelgiud.reversi.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.utils.Utils;

public class TileView extends ImageView {

    private static final int TILE_COLOR_PADDING = (int) Utils.convertDpToPixel(2);

    public static final int BACKGROUND_LIGHT = 0;
    public static final int BACKGROUND_DARK = 1;

    public TileView(Context context) {
        super(context);
        onFinishInflate();
    }

    public TileView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TileView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setPadding(TILE_COLOR_PADDING, TILE_COLOR_PADDING, TILE_COLOR_PADDING, TILE_COLOR_PADDING);
    }

    public void setBackgroundType(final int backgroundType) {
        switch (backgroundType) {
            case BACKGROUND_DARK:
                setBackgroundResource(R.drawable.cell_background_dark);
                break;
            case BACKGROUND_LIGHT:
                setBackgroundResource(R.drawable.cell_background_light);
            default:
                break;
        }
    }

    public void setColor(@NonNull TileColor color) {
        if (color == TileColor.WHITE) {
            setImageResource(R.drawable.white);
            setScaleType(ScaleType.FIT_XY);
        }
        else if (color == TileColor.BLACK) {
            setImageResource(R.drawable.black);
            setScaleType(ScaleType.FIT_XY);
        }
        else {
            removeColor();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int size;
        if(widthMode == MeasureSpec.EXACTLY && widthSize > 0){
            size = widthSize;
        }
        else if(heightMode == MeasureSpec.EXACTLY && heightSize > 0){
            size = heightSize;
        }
        else{
            size = widthSize < heightSize ? widthSize : heightSize;
        }

        int finalMeasureSpec = MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY);
        super.onMeasure(finalMeasureSpec, finalMeasureSpec);
    }

    public void removeColor() {
        setImageResource(android.R.color.transparent);
    }

    public void highlight() {
        setSelected(true);
    }

    public void removeHighlight() {
        setSelected(false);
    }

    public void setPossibleTile() {
        setImageResource(R.drawable.possible);
        setScaleType(ScaleType.CENTER);
    }

}
