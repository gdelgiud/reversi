package com.gdelgiud.reversi.views;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.model.Difficulty;
import com.gdelgiud.reversi.model.Player;
import com.gdelgiud.reversi.model.PlayerType;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlayerStatusView extends RelativeLayout {

    @BindView(R.id.name)
    protected TextView name;
    @BindView(R.id.score)
    protected TextView score;
    @BindView(R.id.difficulty_tag)
    protected TextView difficultyTag;
    @BindView(R.id.color)
    protected ImageView color;
    @BindView(R.id.activeBackground)
    protected View activeBackground;

    boolean reversed = false;

    public PlayerStatusView(Context context) {
        super(context);
        init();
    }

    public PlayerStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        readAttrs(context, attrs);
        init();
    }

    public PlayerStatusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        readAttrs(context, attrs);
        init();
    }

    private void init() {
        int layoutRes = R.layout.player_status_layout;
        if (reversed) {
            layoutRes = R.layout.player_status_reversed_layout;
        }
        LayoutInflater.from(getContext()).inflate(layoutRes, this, true);

        if (isInEditMode()) {
            return;
        }

        ButterKnife.bind(this);
    }

    private void readAttrs(final Context context, final AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PlayerStatusView, 0, 0);
        reversed = a.getBoolean(R.styleable.PlayerStatusView_reversed, false);
        a.recycle();
    }

    public void setPlayer(@NonNull final Player player) {
        name.setText(player.getName());
        score.setText(String.valueOf(player.getScore()));

        if (player.getColor() == TileColor.WHITE) {
            color.setImageResource(R.drawable.white);
            score.setTextColor(getResources().getColor(R.color.primary_text));
        }
        else if (player.getColor() == TileColor.BLACK) {
            color.setImageResource(R.drawable.black);
            score.setTextColor(getResources().getColor(R.color.primary_inverse_text));
        }

        if (player.getType() == PlayerType.HUMAN) {
            difficultyTag.setVisibility(GONE);
            name.setSingleLine(false);
            name.setMaxLines(2);
        }
        else {
            difficultyTag.setVisibility(VISIBLE);
            setDifficulty(player.getDifficulty());
            name.setSingleLine(true);
            name.setMaxLines(1);
        }
    }

    private void setDifficulty(Difficulty difficulty) {
        switch (difficulty) {

            case EASY:
                difficultyTag.setBackgroundResource(R.drawable.cpu_easy_shape);
                break;
            case MEDIUM:
                difficultyTag.setBackgroundResource(R.drawable.cpu_medium_shape);
                break;
            case HARD:
                difficultyTag.setBackgroundResource(R.drawable.cpu_hard_shape);
                break;
        }
    }

    public void setActive(boolean active) {
        activeBackground.setVisibility(VISIBLE);
        float initialAlpha = 0.0f;
        float endAlpha = 1.0f;

        if (!active) {
            initialAlpha = 1.0f;
            endAlpha = 0.0f;
        }

        ObjectAnimator anim = ObjectAnimator.ofFloat(activeBackground, "alpha", initialAlpha, endAlpha);
        anim.setDuration(300);
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.start();
    }

}
