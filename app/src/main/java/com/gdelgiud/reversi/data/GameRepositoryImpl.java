package com.gdelgiud.reversi.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.gdelgiud.reversi.domain.GameRepository;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameRepositoryImpl implements GameRepository {

    private static final String TAG = GameRepositoryImpl.class.getSimpleName();
    private static final String GAME_LIST_FILENAME = "game_list";

    private final SharedPreferences prefs;
    private final Gson gson;
    private long nextId = 0;
    private final HashMap<Long, Game> gamesList = new HashMap<>();

    public GameRepositoryImpl(final Context context, final Gson gson) {
        this.gson = gson;
        this.prefs = context.getSharedPreferences(GAME_LIST_FILENAME, Context.MODE_PRIVATE);
    }

    @Override
    public void addGame(@NonNull final Game game) {
        game.setId(nextId++);
        Log.d(TAG, "Created game with Id " + game.getId());
        persistGame(game);
    }

    @Override
    public boolean updateGame(@NonNull final Game game) {
        if (gamesList.containsKey(game.getId())) {
            persistGame(game);
            Log.d(TAG, "Updated game with Id " + game.getId());
            return true;
        }

        Log.w(TAG, "Game with Id " + game.getId() + " not found");

        return false;
    }

    @Override
    public boolean removeGame(@NonNull final Game game) {
        if (gamesList.containsKey(game.getId())) {
            gamesList.remove(game.getId());
            prefs.edit().remove(String.valueOf(game.getId())).apply();
            Log.d(TAG, "Removed game with Id " + game.getId());
            return true;
        }

        Log.w(TAG, "Game with Id " + game.getId() + " not found");

        return false;
    }

    @Override
    public void removeAllGames() {
        gamesList.clear();
        prefs.edit().clear().apply();
    }

    @Override
    public void loadGames() {
        Map<String, ?> keys = prefs.getAll();

        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            final Game game = gson.fromJson(entry.getValue().toString(), Game.class);
            gamesList.put(game.getId(), game);

            // Set the next game ID with the highest ID found
            if (game.getId() >= nextId) {
                nextId = game.getId() + 1;
            }
        }

        Log.d(TAG, "Loaded " + keys.size() + " games");
    }

    @Override
    public List<Game> getGames() {
        final List<Game> ret = new ArrayList<>();
        ret.addAll(gamesList.values());
        return ret;
    }

    @Override
    public Game createGame(Player whitePlayer, Player blackPlayer) {
        final Game newGame = new Game(whitePlayer, blackPlayer);
        addGame(newGame);
        return newGame;
    }

    @Override
    public boolean hasGames() {
        return !gamesList.isEmpty();
    }

    private void persistGame(@NonNull final Game game) {
        gamesList.put(game.getId(), game);
        prefs.edit().putString(String.valueOf(game.getId()), gson.toJson(game)).apply();
    }
}
