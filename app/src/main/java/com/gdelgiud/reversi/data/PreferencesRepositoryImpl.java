package com.gdelgiud.reversi.data;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.gdelgiud.reversi.domain.PreferencesRepository;
import com.gdelgiud.reversi.injection.scope.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PreferencesRepositoryImpl implements PreferencesRepository {

    private static final String PREF_SHOW_AVAILABLE_MOVES = "show_available_moves";

    private static final boolean PREF_SHOW_AVAILABLE_MOVES_DEFAULT = true;

    private final SharedPreferences sharedPreferences;

    @Inject
    public PreferencesRepositoryImpl(@ApplicationContext Context context) {
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public void setShowAvailableMovesEnabled(boolean enabled) {
        sharedPreferences.edit().putBoolean(PREF_SHOW_AVAILABLE_MOVES, enabled).apply();
    }

    @Override
    public boolean isShowAvailableMovesEnabled() {
        return sharedPreferences.getBoolean(PREF_SHOW_AVAILABLE_MOVES,
                PREF_SHOW_AVAILABLE_MOVES_DEFAULT);
    }
}
