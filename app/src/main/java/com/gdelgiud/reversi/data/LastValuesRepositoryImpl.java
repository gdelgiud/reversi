package com.gdelgiud.reversi.data;


import android.content.Context;
import android.content.SharedPreferences;

import com.gdelgiud.reversi.domain.LastValuesRepository;
import com.gdelgiud.reversi.injection.scope.ApplicationContext;
import com.gdelgiud.reversi.model.Difficulty;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class LastValuesRepositoryImpl implements LastValuesRepository {

    private static final String PREFS_LAST_GAME_VALUES = "PREFS_LAST_GAME_VALUES";
    private static final String PREF_LAST_NAME_P1 = "PREF_LAST_NAME_P1";
    private static final String PREF_LAST_NAME_P2 = "PREF_LAST_NAME_P2";
    private static final String PREF_DIFFICULTY_P1 = "PREF_DIFFICULTY_P1";
    private static final String PREF_DIFFICULTY_P2 = "PREF_DIFFICULTY_P2";

    private final SharedPreferences sharedPreferences;

    @Inject
    public LastValuesRepositoryImpl(@ApplicationContext Context context) {
        sharedPreferences =
                context.getSharedPreferences(PREFS_LAST_GAME_VALUES, Context.MODE_PRIVATE);
    }

    @Override
    public String getLastPlayer1Name() {
        return sharedPreferences.getString(PREF_LAST_NAME_P1, "");
    }

    @Override
    public String getLastPlayer2Name() {
        return sharedPreferences.getString(PREF_LAST_NAME_P2, "");
    }

    @Override
    public void setLastPlayer1Name(String name) {
        sharedPreferences.edit().putString(PREF_LAST_NAME_P1, name).apply();
    }

    @Override
    public void setLastPlayer2Name(String name) {
        sharedPreferences.edit().putString(PREF_LAST_NAME_P2, name).apply();
    }

    @Override
    public void clearLastPlayer1Name() {
        sharedPreferences.edit().remove(PREF_LAST_NAME_P1).apply();
    }

    @Override
    public void clearLastPlayer2Name() {
        sharedPreferences.edit().remove(PREF_LAST_NAME_P2).apply();
    }

    @Override
    public Difficulty getLastPlayer1Difficulty() {
        return getDifficulty(PREF_DIFFICULTY_P1);
    }

    @Override
    public Difficulty getLastPlayer2Difficulty() {
        return getDifficulty(PREF_DIFFICULTY_P2);
    }

    @Override
    public void setLastPlayer1Difficulty(Difficulty difficulty) {
        saveDifficulty(PREF_DIFFICULTY_P1, difficulty);
    }

    @Override
    public void setLastPlayer2Difficulty(Difficulty difficulty) {
        saveDifficulty(PREF_DIFFICULTY_P2, difficulty);
    }

    private Difficulty getDifficulty(String key) {
        return Difficulty.values()[sharedPreferences.getInt(key, Difficulty.MEDIUM.ordinal())];
    }

    private void saveDifficulty(String key, Difficulty difficulty) {
        sharedPreferences.edit().putInt(key, difficulty.ordinal()).apply();
    }
}
