package com.gdelgiud.reversi;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.gdelgiud.reversi.injection.component.ApplicationComponent;
import com.gdelgiud.reversi.injection.component.DaggerApplicationComponent;
import com.gdelgiud.reversi.injection.module.ApplicationModule;
import com.squareup.leakcanary.LeakCanary;

public class ReversiApp extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        LeakCanary.install(this);
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public static ReversiApp get(@NonNull Context context) {
        return (ReversiApp) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }
}
