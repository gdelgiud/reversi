package com.gdelgiud.reversi.ui.game;


import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.jversi.Turn;
import com.gdelgiud.reversi.domain.GameHandler;
import com.gdelgiud.reversi.domain.PreferencesRepository;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;
import com.gdelgiud.reversi.ui.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

public class GamePresenter extends BasePresenter<GameView> {

    private final PreferencesRepository preferencesRepository;
    private final GameHandler gameHandler;
    private final DisposableObserver<Game> gameObserver;

    @Inject
    public GamePresenter(GameHandler gameHandler, PreferencesRepository preferencesRepository) {
        this.gameHandler = gameHandler;
        this.preferencesRepository = preferencesRepository;
        gameObserver = new DisposableObserver<Game>() {

            @Override
            public void onNext(Game game) {
                showGame(game);
            }

            @Override
            public void onError(Throwable e) {
                if (e instanceof OutOfMemoryError) {
                    getView().showOutOfMemoryError();
                }
                else {
                    getView().showUnknownError();
                }
                getView().closeUi();
            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public void detachView() {
        super.detachView();

        if (!gameHandler.getGame().isFinished()) {
            gameHandler.saveGame();
        }

        gameObserver.dispose();
    }

    public void viewResumed() {
        gameHandler.getGameObservable().subscribe(gameObserver);
    }

    public void setGame(Game game) {
        gameHandler.setGame(game);
    }

    public Game getGame() {
        return gameHandler.getGame();
    }

    public void selectTile(int col, int row) {
        if (gameHandler.isPlayerControllable()) {
            getView().highlightTile(col, row);
        }
    }

    public void releaseTile(int col, int row) {
        final BoardPosition position = new BoardPosition(col, row);
        if (gameHandler.isPlayerControllable() && gameHandler.isValidMove(position)) {
            playTurn(position);
        }
        getView().removeHighlight();
    }

    public void cancelMove() {
        getView().removeHighlight();
    }

    public void playTurn(BoardPosition position) {
        gameHandler.playTile(position);
    }

    public void showGame(final Game game) {
        getView().showBoard(game.getBoard());
        getView().showWhitePlayer(game.getWhitePlayer());
        getView().showBlackPlayer(game.getBlackPlayer());
        if (!game.isFinished()) {
            final Player currentPlayer = gameHandler.getGame().getCurrentPlayer();
            if (currentPlayer.getColor() == TileColor.WHITE) {
                getView().showWhitePlayerActive();
            }
            else {
                getView().showBlackPlayerActive();
            }
            if (gameHandler.isPlayerControllable()) {
                showHumanPlayerTurn(currentPlayer);
            } else {
                showAiPlayerTurn(currentPlayer);
            }
            gameHandler.processTurn();
        } else {
            showGameEnd();
        }
    }

    public void showHint() {
        if (gameHandler.isPlayerControllable()) {
            gameHandler.hint().subscribe(new SingleObserver<Turn>() {
                @Override
                public void onSubscribe(Disposable d) {}

                @Override
                public void onSuccess(Turn hint) {
                    processHint(hint);
                }

                @Override
                public void onError(Throwable e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public void passTurn() {
        if (gameHandler.isPlayerControllable()) {
            getView().showPassConfirmation();
        }
    }

    public void confirmPassTurn() {
        if (gameHandler.isPlayerControllable()) {
            gameHandler.passTurn();
        }
    }

    public void resign() {
        if (gameHandler.isPlayerControllable()) {
            getView().showResignConfirmation();
        }
    }

    public void confirmResign() {
        if (gameHandler.isPlayerControllable()) {
            gameHandler.resign();
        }
    }

    public void rematch() {
        gameHandler.newGame();
    }

    void processHint(Turn hint) {
        switch (hint.getType()) {
            case PLAY:
                getView().showHint(hint.getMove());
                break;
            case PASS:
                getView().highlightPassButton();
                break;
            case RESIGN:
                break;
        }
    }

    private void showHumanPlayerTurn(Player humanPlayer) {
        final List<BoardPosition> availableMoves = gameHandler.getAvailableMoves();
        final TileColor currentPlayerColor = humanPlayer.getColor();
        final boolean hasAvailableMoves = !availableMoves.isEmpty();
        if (hasAvailableMoves && preferencesRepository.isShowAvailableMovesEnabled()) {
            getView().showAvailableMoves(availableMoves, currentPlayerColor);
        }
        getView().setHintEnabled(hasAvailableMoves);
        getView().showPlayerControls();
    }

    private void showAiPlayerTurn(Player aiPlayer) {
        getView().showWaitingForAi(aiPlayer.getName());
    }

    private void showGameEnd() {
        getView().playerVictory(gameHandler.getWinningPlayer().getName());
        getView().showRematchButton();
        getView().clearActivePlayer();
        gameHandler.removeGame();
    }
}
