package com.gdelgiud.reversi.ui.main;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;

import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.ui.newgame.NewGameActivity;
import com.gdelgiud.reversi.ui.saves.SavedGamesActivity;
import com.gdelgiud.reversi.ui.settings.SettingsActivity;
import com.gdelgiud.reversi.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainView {

    @Inject
    MainPresenter actions;

    @BindView(R.id.button_saved_games)
    protected Button savedGamesButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        ButterKnife.bind(this);
        actions.attachView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        actions.viewResumed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        actions.detachView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                SettingsActivity.getIntent(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public void configureActionBar(@NonNull ActionBar actionBar) {
        super.configureActionBar(actionBar);
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void setSavedGamesEnabled(boolean enabled) {
        savedGamesButton.setEnabled(enabled);
    }

    @Override
    public void navigateToNewSinglePlayerGame() {
        startActivity(NewGameActivity.getIntent(this, NewGameActivity.GAME_SINGLE_PLAYER));
    }

    @Override
    public void navigateToNewTwoPlayersGame() {
        startActivity(NewGameActivity.getIntent(this, NewGameActivity.GAME_TWO_PLAYERS));
    }

    @Override
    public void navigateToWatchGame() {
        startActivity(NewGameActivity.getIntent(this, NewGameActivity.GAME_WATCH));
    }

    @Override
    public void navigateToSavedGames() {
        startActivity(SavedGamesActivity.getIntent(this));
    }

    @Override
    public void navigateToSettings() {
        startActivity(SettingsActivity.getIntent(this));
    }

    @OnClick(R.id.button_single_player)
    public void singlePlayerClicked() {
        actions.newSinglePlayerGame();
    }

    @OnClick(R.id.button_two_players)
    public void twoPlayersClicked() {
        actions.newTwoPlayersGame();
    }

    @OnClick(R.id.button_watch)
    public void watchClicked() {
        actions.watchGame();
    }

    @OnClick(R.id.button_saved_games)
    public void savedGames() {
        actions.openSavedGames();
    }

    @OnClick(R.id.settings_button)
    public void settingsClicked() {
        actions.openSettings();
    }

}
