package com.gdelgiud.reversi.ui.saves;


import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.ui.base.ViewContract;

import java.util.List;

public interface SavedGamesView extends ViewContract {
    void setGames(List<Game> games);
    void showDeleteGamesConfirmation();
    void navigateToGameScreen(Game game);
    void closeUi();
}
