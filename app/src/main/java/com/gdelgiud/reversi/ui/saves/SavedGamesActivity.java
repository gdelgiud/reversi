package com.gdelgiud.reversi.ui.saves;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.ui.game.GameActivity;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.ui.base.BaseActivity;
import com.gdelgiud.reversi.views.decoration.DividerItemDecoration;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SavedGamesActivity extends BaseActivity implements SavedGamesView,
        GameListAdapter.OnGameItemClickListener {

    @Inject
    SavedGamesPresenter actions;
    @Inject
    GameListAdapter gameListAdapter;

    @BindView(R.id.gameList)
    protected RecyclerView gameList;

    public static Intent getIntent(final Context context) {
        return new Intent(context, SavedGamesActivity.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        ButterKnife.bind(this);

        gameListAdapter.setOnGameItemClickListener(this);
        gameList.setLayoutManager(new LinearLayoutManager(this));
        gameList.setAdapter(gameListAdapter);
        gameList.addItemDecoration(new DividerItemDecoration(this, null, false, true));

        actions.attachView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        actions.viewResumed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        actions.detachView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.delete_games_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.delete_games:
                actions.removeAllGames();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.saved_games_activity;
    }

    @Override
    public void setGames(List<Game> games) {
        gameListAdapter.setGames(games);
        gameListAdapter.notifyDataSetChanged();
    }

    @Override
    public void navigateToGameScreen(Game game) {
        startActivity(GameActivity.getIntent(this, game));
    }

    @Override
    public void closeUi() {
        finish();
    }

    @Override
    public void showDeleteGamesConfirmation() {
        DialogInterface.OnClickListener dialogClickListener =new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        actions.confirmRemoveAllGames();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.clear_saved_games_confirmation)
                .setPositiveButton(R.string.clear, dialogClickListener)
                .setNegativeButton(R.string.cancel, dialogClickListener).show();
    }

    @Override
    public void onGameSelected(final Game game) {
        actions.selectGame(game);
    }
}
