package com.gdelgiud.reversi.ui.game;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.gdelgiud.jversi.Board;
import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;
import com.gdelgiud.reversi.ui.base.BaseActivity;
import com.gdelgiud.reversi.utils.Utils;
import com.gdelgiud.reversi.views.BoardLayout;
import com.gdelgiud.reversi.views.PlayerStatusView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class GameActivity extends BaseActivity implements GameView {

    public static final String GAME_EXTRA = "game_extra";

    private static final int BOTTOM_BAR_NORMAL_INDEX = 0;
    private static final int BOTTOM_BAR_AI_TURN_INDEX = 1;

    @Inject
    GamePresenter actions;

    @BindView(R.id.board)
    BoardLayout boardLayout;
    @BindView(R.id.whitePlayerStatus)
    PlayerStatusView whitePlayerStatus;
    @BindView(R.id.blackPlayerStatus)
    PlayerStatusView blackPlayerStatus;
    @BindView(R.id.hintButton)
    Button hintButton;
    @BindView(R.id.passButton)
    Button passButton;
    @BindView(R.id.resignButton)
    Button resignButton;
    @BindView(R.id.rematchButton)
    Button rematchButton;
    @BindView(R.id.bottomBarViewSwitcher)
    ViewSwitcher bottomBarViewSwitcher;
    @BindView(R.id.aiPlayingText)
    TextView aiPlayingText;

    public static Intent getIntent(Context context, Game game) {
        final Intent intent = new Intent(context, GameActivity.class);
        intent.putExtra(GAME_EXTRA, game);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getComponent().inject(this);
        Game game = (Game) getIntent().getExtras().getSerializable(GAME_EXTRA);
        boardLayout.setBoardEventListener(new BoardLayout.BoardEventListener() {
            @Override
            public void onTileSelected(int column, int row) {
                actions.selectTile(column, row);
            }

            @Override
            public void onBoardReleased(int column, int row) {
                actions.releaseTile(column, row);
            }

            @Override
            public void onCancel() {
                actions.cancelMove();
            }
        });
        actions.attachView(this);
        actions.setGame(game);
    }

    @Override
    protected void onResume() {
        super.onResume();
        actions.viewResumed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        actions.detachView();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.game_activity;
    }

    @Override
    public void showBoard(Board board) {
        boardLayout.setBoard(board);
    }

    @Override
    public void showWhitePlayer(Player player) {
        whitePlayerStatus.setPlayer(player);
    }

    @Override
    public void showBlackPlayer(Player player) {
        blackPlayerStatus.setPlayer(player);
    }

    @Override
    public void highlightTile(int col, int row) {
        boardLayout.highlightTile(col, row);
    }

    @Override
    public void removeHighlight() {
        boardLayout.removeHighlight();
    }

    @Override
    public void showAvailableMoves(List<BoardPosition> possibleMoves, TileColor color) {
        boardLayout.displayValidMoves(possibleMoves, color);
    }

    @Override
    public void showPlayerControls() {
        hintButton.setVisibility(VISIBLE);
        passButton.setVisibility(VISIBLE);
        resignButton.setVisibility(VISIBLE);
        rematchButton.setVisibility(GONE);
        bottomBarViewSwitcher.setDisplayedChild(BOTTOM_BAR_NORMAL_INDEX);
    }

    @Override
    public void showHint(BoardPosition position) {
        boardLayout.highlightTile(position.x, position.y);
    }

    @Override
    public void setHintEnabled(boolean enabled) {
        hintButton.setEnabled(enabled);
    }

    @Override
    public void showPassConfirmation() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        actions.confirmPassTurn();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.pass_turn)
                .setMessage(R.string.are_you_sure)
                .setPositiveButton(R.string.yes, dialogClickListener)
                .setNegativeButton(R.string.no, dialogClickListener)
                .show();
    }

    @Override
    public void showResignConfirmation() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        actions.confirmResign();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.resign)
                .setMessage(R.string.are_you_sure)
                .setPositiveButton(R.string.yes, dialogClickListener)
                .setNegativeButton(R.string.no, dialogClickListener)
                .show();
    }

    @Override
    public void showWhitePlayerActive() {
        whitePlayerStatus.setActive(true);
        blackPlayerStatus.setActive(false);
    }

    @Override
    public void showBlackPlayerActive() {
        whitePlayerStatus.setActive(false);
        blackPlayerStatus.setActive(true);
    }

    @Override
    public void clearActivePlayer() {
        whitePlayerStatus.setActive(false);
        blackPlayerStatus.setActive(false);
    }

    @Override
    public void showUnknownError() {
        Utils.showLongToast(this, R.string.error_unexpected);
    }

    @Override
    public void showOutOfMemoryError() {
        Utils.showLongToast(this, R.string.error_out_of_memory);
    }

    @Override
    public void closeUi() {

    }

    @Override
    public void playerVictory(String playerName) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.match_ended)
                .setMessage(getString(R.string.last_turn_won, playerName))
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    @Override
    public void showRematchButton() {
        hintButton.setVisibility(GONE);
        passButton.setVisibility(GONE);
        resignButton.setVisibility(GONE);
        rematchButton.setVisibility(VISIBLE);
        bottomBarViewSwitcher.setDisplayedChild(BOTTOM_BAR_NORMAL_INDEX);
    }

    @Override
    public void highlightPassButton() {
        hintButton.setSelected(true);
    }

    @Override
    public void showWaitingForAi(String playerName) {
        bottomBarViewSwitcher.setDisplayedChild(BOTTOM_BAR_AI_TURN_INDEX);
        aiPlayingText.setText(getString(R.string.its_players_turn, playerName));
    }

    @OnClick(R.id.hintButton)
    public void onHintButtonClicked() {
        actions.showHint();
    }

    @OnClick(R.id.passButton)
    public void onPassButtonClicked() {
        actions.passTurn();
    }

    @OnClick(R.id.resignButton)
    public void onResignButtonClicked() {
        actions.resign();
    }

    @OnClick(R.id.rematchButton)
    public void onRematchButtonClicked() {
        actions.rematch();
    }

}
