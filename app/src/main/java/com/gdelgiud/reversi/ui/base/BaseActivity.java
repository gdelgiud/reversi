package com.gdelgiud.reversi.ui.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.ReversiApp;
import com.gdelgiud.reversi.injection.component.ActivityComponent;
import com.gdelgiud.reversi.injection.component.DaggerActivityComponent;
import com.gdelgiud.reversi.injection.module.ActivityModule;

public abstract class BaseActivity extends AppCompatActivity {

    private ActivityComponent activityComponent;

    public ActivityComponent getComponent() {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(ReversiApp.get(this).getComponent())
                    .build();
        }
        return activityComponent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final int layoutResId = getLayoutRes();
        if (layoutResId > 0) {
            setContentView(layoutResId);
        }

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            configureActionBar(getSupportActionBar());
        }
    }

    public abstract int getLayoutRes();

    public void configureActionBar(@NonNull final ActionBar actionBar) {
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

}
