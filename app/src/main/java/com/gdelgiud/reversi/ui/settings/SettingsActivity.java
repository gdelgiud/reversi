package com.gdelgiud.reversi.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.ui.base.BaseActivity;

public class SettingsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_settings;
    }

    public static Intent getIntent(final Context context) {
        return new Intent(context, SettingsActivity.class);
    }

}
