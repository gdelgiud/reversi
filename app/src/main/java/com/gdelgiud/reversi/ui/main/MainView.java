package com.gdelgiud.reversi.ui.main;


import com.gdelgiud.reversi.ui.base.ViewContract;

public interface MainView extends ViewContract {
    void setSavedGamesEnabled(boolean enabled);
    void navigateToNewSinglePlayerGame();
    void navigateToNewTwoPlayersGame();
    void navigateToWatchGame();
    void navigateToSavedGames();
    void navigateToSettings();
}
