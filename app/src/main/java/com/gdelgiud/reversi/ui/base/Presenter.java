package com.gdelgiud.reversi.ui.base;


public interface Presenter<V extends ViewContract> {

    void attachView(V view);

    void detachView();
}
