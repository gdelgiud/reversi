package com.gdelgiud.reversi.ui.newgame;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.ui.game.GameActivity;
import com.gdelgiud.reversi.model.Difficulty;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.PlayerType;
import com.gdelgiud.reversi.ui.base.BaseActivity;
import com.gdelgiud.reversi.ui.settings.SettingsActivity;
import com.gdelgiud.reversi.views.PlayerSetupView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewGameActivity extends BaseActivity implements NewGameView {

    public static final String GAME_TYPE_EXTRA = "game_type_extra";

    public static final int GAME_SINGLE_PLAYER = 0;
    public static final int GAME_TWO_PLAYERS = 1;
    public static final int GAME_WATCH = 2;

    @Inject
    NewGamePresenter actions;

    @BindView(R.id.player_1_setup)
    protected PlayerSetupView playerSetup1;
    @BindView(R.id.player_2_setup)
    protected PlayerSetupView playerSetup2;

    @Override
    public int getLayoutRes() {
        return R.layout.new_game_activity;
    }

    public static Intent getIntent(final Context context, int gameType) {
        final Intent intent = new Intent(context, NewGameActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.putExtra(GAME_TYPE_EXTRA, gameType);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getComponent().inject(this);
        ButterKnife.bind(this);
        actions.attachView(this);

        if (savedInstanceState != null) {
            handleArguments(savedInstanceState);
        }
        else {
            handleArguments(getIntent().getExtras());
        }

        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        actions.detachView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_settings:
                SettingsActivity.getIntent(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public void showSinglePlayerMode() {
        playerSetup1.setTitle(R.string.you);
        playerSetup2.setTitle(R.string.opponent);
        playerSetup1.setPlayerColor(TileColor.WHITE);
        playerSetup2.setPlayerColor(TileColor.BLACK);
        playerSetup1.setPlayerType(PlayerType.HUMAN);
        playerSetup2.setPlayerType(PlayerType.COMPUTER);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(R.string.single_player);
        }
    }

    @Override
    public void showTwoPlayersMode() {
        playerSetup1.setTitle(R.string.player1);
        playerSetup2.setTitle(R.string.player2);
        playerSetup1.setPlayerColor(TileColor.WHITE);
        playerSetup2.setPlayerColor(TileColor.BLACK);
        playerSetup1.setPlayerType(PlayerType.HUMAN);
        playerSetup2.setPlayerType(PlayerType.HUMAN);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(R.string.two_players);
        }
    }

    @Override
    public void showWatchMode() {
        playerSetup1.setTitle(R.string.player1);
        playerSetup2.setTitle(R.string.player2);
        playerSetup1.setPlayerColor(TileColor.WHITE);
        playerSetup2.setPlayerColor(TileColor.BLACK);
        playerSetup1.setPlayerType(PlayerType.COMPUTER);
        playerSetup2.setPlayerType(PlayerType.COMPUTER);
        playerSetup1.setComputerDifficulty(Difficulty.MEDIUM);
        playerSetup2.setComputerDifficulty(Difficulty.MEDIUM);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(R.string.watch);
        }
    }

    @Override
    public void setPlayer1Name(String name) {
        playerSetup1.setName(name);
    }

    @Override
    public void setPlayer2Name(String name) {
        playerSetup2.setName(name);
    }

    @Override
    public void setPlayer1Color(TileColor color) {
        playerSetup1.setPlayerColor(color);
    }

    @Override
    public void setPlayer2Color(TileColor color) {
        playerSetup2.setPlayerColor(color);
    }

    @Override
    public void setPlayer1Difficulty(Difficulty difficulty) {
        playerSetup1.setComputerDifficulty(difficulty);
    }

    @Override
    public void setPlayer2Difficulty(Difficulty difficulty) {
        playerSetup2.setComputerDifficulty(difficulty);
    }

    @Override
    public void navigateToGameScreen(Game game) {
        startActivity(GameActivity.getIntent(this, game));
    }

    private void handleArguments(final Bundle args) {
        if (args != null && args.containsKey(GAME_TYPE_EXTRA)) {
            switch (args.getInt(GAME_TYPE_EXTRA)) {
                case GAME_SINGLE_PLAYER:
                    actions.selectSinglePlayerMode();
                    break;
                case GAME_TWO_PLAYERS:
                    actions.selectTwoPlayersMode();
                    break;
                case GAME_WATCH:
                    actions.selectWatchMode();
                    break;
            }
        }
    }

    protected void init() {
        playerSetup1.generateRandomName();
        playerSetup2.generateRandomName();

        playerSetup1.setCallback(new PlayerSetupView.Callback() {
            @Override
            public void onColorSelected(TileColor color) {
                actions.selectPlayer1Color(color.opposing());
            }
        });

        playerSetup2.setCallback(new PlayerSetupView.Callback() {
            @Override
            public void onColorSelected(TileColor color) {
                actions.selectPlayer2Color(color.opposing());
            }
        });
    }

    @OnClick(R.id.start_game_button)
    protected void startGameButtonClicked() {
        actions.startGame(playerSetup1.getPlayer(), playerSetup2.getPlayer());
    }

}
