package com.gdelgiud.reversi.ui.main;


import com.gdelgiud.reversi.domain.GameRepository;
import com.gdelgiud.reversi.ui.base.BasePresenter;

import javax.inject.Inject;

public class MainPresenter extends BasePresenter<MainView> {

    private final GameRepository gameRepository;

    @Inject
    public MainPresenter(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Override
    public void attachView(MainView view) {
        super.attachView(view);
        loadGames();
    }

    public void viewResumed() {
        getView().setSavedGamesEnabled(gameRepository.hasGames());
    }

    public void loadGames() {
        gameRepository.loadGames();
    }

    public void newSinglePlayerGame() {
        getView().navigateToNewSinglePlayerGame();
    }

    public void newTwoPlayersGame() {
        getView().navigateToNewTwoPlayersGame();
    }

    public void watchGame() {
        getView().navigateToWatchGame();
    }

    public void openSavedGames() {
        if (gameRepository.hasGames()) {
            getView().navigateToSavedGames();
        }
    }

    public void openSettings() {
        getView().navigateToSettings();
    }

}
