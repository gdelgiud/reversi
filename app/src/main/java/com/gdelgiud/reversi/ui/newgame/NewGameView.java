package com.gdelgiud.reversi.ui.newgame;


import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.reversi.model.Difficulty;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.ui.base.ViewContract;

public interface NewGameView extends ViewContract {
    void showSinglePlayerMode();
    void showTwoPlayersMode();
    void showWatchMode();
    void setPlayer1Name(String name);
    void setPlayer2Name(String name);
    void setPlayer1Color(TileColor color);
    void setPlayer2Color(TileColor color);
    void setPlayer1Difficulty(Difficulty difficulty);
    void setPlayer2Difficulty(Difficulty difficulty);
    void navigateToGameScreen(Game game);
}
