package com.gdelgiud.reversi.ui.newgame;


import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.jversi.ai.AIConfig;
import com.gdelgiud.reversi.domain.GameRepository;
import com.gdelgiud.reversi.domain.LastValuesRepository;
import com.gdelgiud.reversi.model.Difficulty;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;
import com.gdelgiud.reversi.model.PlayerSetupModel;
import com.gdelgiud.reversi.model.PlayerType;
import com.gdelgiud.reversi.ui.base.BasePresenter;

import javax.inject.Inject;

public class NewGamePresenter extends BasePresenter<NewGameView> {

    private final LastValuesRepository lastValuesRepository;
    private final GameRepository gameRepository;

    @Inject
    public NewGamePresenter(LastValuesRepository lastValuesRepository, GameRepository gameRepository) {
        this.lastValuesRepository = lastValuesRepository;
        this.gameRepository = gameRepository;
    }

    public void selectSinglePlayerMode() {
        getView().showSinglePlayerMode();
        getView().setPlayer1Name(lastValuesRepository.getLastPlayer1Name());
        getView().setPlayer2Difficulty(lastValuesRepository.getLastPlayer2Difficulty());
    }

    public void selectTwoPlayersMode() {
        getView().showTwoPlayersMode();
        getView().setPlayer1Name(lastValuesRepository.getLastPlayer1Name());
        getView().setPlayer2Name(lastValuesRepository.getLastPlayer2Name());
    }

    public void selectWatchMode() {
        getView().showWatchMode();
        getView().setPlayer1Difficulty(lastValuesRepository.getLastPlayer1Difficulty());
        getView().setPlayer2Difficulty(lastValuesRepository.getLastPlayer2Difficulty());
    }

    public void selectPlayer1Color(TileColor color) {
        getView().setPlayer2Color(color);
    }

    public void selectPlayer2Color(TileColor color) {
        getView().setPlayer1Color(color);
    }

    public void startGame(PlayerSetupModel player1Model, PlayerSetupModel player2Model) {
        // TODO Get this out of the presenter
        final Player player1 = new Player(player1Model.getName(), player1Model.getColor());
        final Player player2 = new Player(player2Model.getName(), player2Model.getColor());
        Game game;

        if (player1Model.getType() == PlayerType.COMPUTER) {
            player1.setAIConfig(getAiConfig(player1Model.getDifficulty()));
        }
        if (player2Model.getType() == PlayerType.COMPUTER) {
            player2.setAIConfig(getAiConfig(player2Model.getDifficulty()));
        }
        player1.setType(player1Model.getType());
        player2.setType(player2Model.getType());
        player1.setDifficulty(player1Model.getDifficulty());
        player2.setDifficulty(player2Model.getDifficulty());

        if (player1.getColor() == TileColor.WHITE) {
            game = new Game(player1, player2);
        }
        else {
            game = new Game(player2, player1);
        }

        gameRepository.addGame(game);
        persistValues(player1Model, player2Model);
        getView().navigateToGameScreen(game);
    }

    private AIConfig getAiConfig(Difficulty difficulty) {
        // TODO Get this out of the presenter
        AIConfig aiConfig = new AIConfig();
        int depth;
        switch (difficulty) {
            case EASY:
                depth = 3;
                break;
            default:
            case MEDIUM:
                depth = 4;
                break;
            case HARD:
                depth = 5;
                break;
        }
        aiConfig.setDepthLimited(depth);
        return aiConfig;
    }

    private void persistValues(PlayerSetupModel player1, PlayerSetupModel player2) {
        // TODO Get this out of the presenter
        // Persist player 1 data
        if (player1.getType() == PlayerType.COMPUTER) {
            lastValuesRepository.setLastPlayer1Difficulty(player1.getDifficulty());
        }
        else if (!player1.isRandomName()) {
            lastValuesRepository.setLastPlayer1Name(player1.getName());
        }
        else {
            lastValuesRepository.clearLastPlayer1Name();
        }

        // Persist player 2 data
        if (player2.getType() == PlayerType.COMPUTER) {
            lastValuesRepository.setLastPlayer2Difficulty(player2.getDifficulty());
        }
        else if (!player2.isRandomName()) {
            lastValuesRepository.setLastPlayer2Name(player2.getName());
        }
        else {
            lastValuesRepository.clearLastPlayer2Name();
        }
    }
}
