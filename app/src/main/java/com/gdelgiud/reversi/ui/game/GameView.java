package com.gdelgiud.reversi.ui.game;


import com.gdelgiud.jversi.Board;
import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.reversi.model.Player;
import com.gdelgiud.reversi.ui.base.ViewContract;

import java.util.List;

public interface GameView extends ViewContract {
    void showBoard(Board board);
    void showWhitePlayer(Player player);
    void showBlackPlayer(Player player);
    void highlightTile(int col, int row);
    void removeHighlight();
    void showAvailableMoves(List<BoardPosition> possibleMoves, TileColor color);
    void showPlayerControls();
    void showHint(BoardPosition position);
    void setHintEnabled(boolean enabled);
    void showPassConfirmation();
    void showResignConfirmation();
    void playerVictory(String playerName);
    void showRematchButton();
    void highlightPassButton();
    void showWaitingForAi(String playerName);
    void showWhitePlayerActive();
    void showBlackPlayerActive();
    void clearActivePlayer();
    void showUnknownError();
    void showOutOfMemoryError();
    void closeUi();
}
