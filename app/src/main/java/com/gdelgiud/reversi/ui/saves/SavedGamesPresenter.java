package com.gdelgiud.reversi.ui.saves;


import com.gdelgiud.reversi.domain.GameRepository;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.ui.base.BasePresenter;

import javax.inject.Inject;

public class SavedGamesPresenter extends BasePresenter<SavedGamesView> {

    private final GameRepository gameRepository;

    @Inject
    public SavedGamesPresenter(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public void viewResumed() {
        if (!gameRepository.hasGames()) {
            getView().closeUi();
        }
        else {
            getView().setGames(gameRepository.getGames());
        }
    }

    public void removeAllGames() {
        getView().showDeleteGamesConfirmation();
    }

    public void confirmRemoveAllGames() {
        gameRepository.removeAllGames();
        getView().closeUi();
    }

    public void selectGame(Game game) {
        getView().navigateToGameScreen(game);
    }
}
