package com.gdelgiud.reversi.ui.saves;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;
import com.gdelgiud.reversi.model.PlayerType;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GameListAdapter extends RecyclerView.Adapter<GameListAdapter.GameItemViewHolder> {

    private final List<Game> games = new ArrayList<>();
    private final LayoutInflater layoutInflater;
    private OnGameItemClickListener listener;

    public interface OnGameItemClickListener {
        void onGameSelected(final Game game);
    }

    @Inject
    public GameListAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    public void setGames(@Nullable final List<Game> games) {
        this.games.clear();
        if (games != null) {
            this.games.addAll(games);
        }
    }

    public void setOnGameItemClickListener(final OnGameItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public GameItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GameItemViewHolder(layoutInflater
                .inflate(R.layout.item_saved_game, parent, false));
    }

    @Override
    public void onBindViewHolder(GameItemViewHolder holder, int position) {
        holder.update(games.get(position));
    }

    @Override
    public int getItemCount() {
        return games.size();
    }

    class GameItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_white_score)
        TextView whiteScore;
        @BindView(R.id.text_black_score)
        TextView blackScore;
        @BindView(R.id.text_white_player_name)
        TextView whiteName;
        @BindView(R.id.text_black_player_name)
        TextView blackName;
        @BindView(R.id.text_white_player_difficulty)
        TextView whiteDifficulty;
        @BindView(R.id.text_black_player_difficulty)
        TextView blackDifficulty;

        GameItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void update(final Game game) {
            final Player white = game.getWhitePlayer();
            final Player black = game.getBlackPlayer();

            // Update info
            whiteScore.setText(String.valueOf(white.getScore()));
            blackScore.setText(String.valueOf(black.getScore()));
            whiteName.setText(white.getName());
            blackName.setText(black.getName());

            if (game.getWhitePlayer().getType() == PlayerType.COMPUTER) {
                whiteDifficulty.setVisibility(View.VISIBLE);
                switch (game.getWhitePlayer().getDifficulty()) {

                    case EASY:
                        whiteDifficulty.setBackgroundResource(R.drawable.cpu_easy_shape);
                        break;
                    case MEDIUM:
                        whiteDifficulty.setBackgroundResource(R.drawable.cpu_medium_shape);
                        break;
                    case HARD:
                        whiteDifficulty.setBackgroundResource(R.drawable.cpu_hard_shape);
                        break;
                }
            }
            else {
                whiteDifficulty.setVisibility(View.GONE);
            }

            if (game.getBlackPlayer().getType() == PlayerType.COMPUTER) {
                blackDifficulty.setVisibility(View.VISIBLE);
                switch (game.getBlackPlayer().getDifficulty()) {
                    case EASY:
                        blackDifficulty.setBackgroundResource(R.drawable.cpu_easy_shape);
                        break;
                    case MEDIUM:
                        blackDifficulty.setBackgroundResource(R.drawable.cpu_medium_shape);
                        break;
                    case HARD:
                        blackDifficulty.setBackgroundResource(R.drawable.cpu_hard_shape);
                        break;
                }
            }
            else {
                blackDifficulty.setVisibility(View.GONE);
            }

            // Set current turn
            if (game.getCurrentColor() == TileColor.WHITE) {
                whiteName.setTypeface(null, Typeface.BOLD);
                blackName.setTypeface(null, Typeface.NORMAL);
            }
            else if (game.getCurrentColor() == TileColor.BLACK) {
                whiteName.setTypeface(null, Typeface.NORMAL);
                blackName.setTypeface(null, Typeface.BOLD);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onGameSelected(game);
                    }
                }
            });
        }
    }
}
