package com.gdelgiud.reversi.model;

import android.support.annotation.NonNull;

import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.ReversiGame;
import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.jversi.exception.InvalidMoveException;
import com.gdelgiud.jversi.exception.WrongPlayerTurnException;

public class Game extends ReversiGame {

    private long id = -1;
    private Player whitePlayer;
    private Player blackPlayer;

    public Game(final Player whitePlayer, final Player blackPlayer) {
        this.whitePlayer = whitePlayer;
        this.blackPlayer = blackPlayer;

        refreshScore();
    }

    public Player getWhitePlayer() {
        return whitePlayer;
    }

    public Player getBlackPlayer() {
        return blackPlayer;
    }

    public Player getCurrentPlayer() {
        return getPlayer(getCurrentColor());
    }

    public Player getPlayer(@NonNull final TileColor color) {
        Player player = null;
        switch (color) {
            case WHITE:
                player = getWhitePlayer();
                break;
            case BLACK:
                player = getBlackPlayer();
                break;
        }
        return player;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void refreshScore() {
        whitePlayer.setScore(getBoard().getWhiteTiles());
        blackPlayer.setScore(getBoard().getBlackTiles());
    }

    @Override
    public void playTurn(TileColor color, BoardPosition position) throws InvalidMoveException,
            WrongPlayerTurnException {
        super.playTurn(color, position);

        refreshScore();
    }
}
