package com.gdelgiud.reversi.model;

public enum PlayerType {
    HUMAN,
    COMPUTER
}
