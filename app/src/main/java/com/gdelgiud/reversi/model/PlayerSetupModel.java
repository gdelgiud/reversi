package com.gdelgiud.reversi.model;


import com.gdelgiud.jversi.TileColor;

public class PlayerSetupModel {

    private final String name;
    private final Difficulty difficulty;
    private final PlayerType type;
    private final TileColor color;
    private final boolean randomName;

    public PlayerSetupModel(String name, Difficulty difficulty, PlayerType type, TileColor color,
                            boolean randomName) {
        this.name = name;
        this.difficulty = difficulty;
        this.type = type;
        this.color = color;
        this.randomName = randomName;
    }

    public String getName() {
        return name;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public PlayerType getType() {
        return type;
    }

    public TileColor getColor() {
        return color;
    }

    public boolean isRandomName() {
        return randomName;
    }
}
