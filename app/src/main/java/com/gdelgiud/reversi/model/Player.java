package com.gdelgiud.reversi.model;

import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.jversi.ai.AIConfig;

import java.io.Serializable;

public class Player implements Serializable {

    private final String name;
    private final TileColor color;
    private int score;
    private Difficulty difficulty;
    private PlayerType type;

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public PlayerType getType() {
        return type;
    }

    public void setType(PlayerType type) {
        this.type = type;
    }

    private AIConfig aiConfig;

    public Player(String name, TileColor color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public TileColor getColor() {
        return color;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        if (score < 0) {
            score = 0;
        }

        this.score = score;
    }

    public void setAIConfig(final AIConfig aiConfig) {
        this.aiConfig = aiConfig;
    }

    /**
     * Tells whether the Player is controllable (neither AI nor remote Player) or not
     * @return
     */
    public boolean isControllable() {
        return aiConfig == null;
    }

    public AIConfig getAiConfig() {
        return aiConfig;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return name.equals(player.name) && color == player.color;

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + color.hashCode();
        return result;
    }
}
