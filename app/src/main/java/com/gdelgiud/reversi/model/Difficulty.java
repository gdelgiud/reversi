package com.gdelgiud.reversi.model;

public enum Difficulty {
    EASY,
    MEDIUM,
    HARD
}
