package com.gdelgiud.reversi.domain.impl;

import com.gdelgiud.jversi.Board;
import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.jversi.Turn;
import com.gdelgiud.jversi.ai.AIConfig;
import com.gdelgiud.jversi.ai.Minimax;
import com.gdelgiud.jversi.exception.InvalidMoveException;
import com.gdelgiud.jversi.exception.WrongPlayerTurnException;
import com.gdelgiud.reversi.domain.GameRepository;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GameHandlerImplTest {

    private GameHandlerImpl gameHandler;

    @Mock
    private GameRepository gameRepository;
    @Mock
    private Game game;
    @Mock
    private Board board;
    @Mock
    private Minimax minimax;
    @Mock
    private AIConfig aiConfig;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        gameHandler = new GameHandlerImpl(gameRepository,
                minimax, aiConfig, Schedulers.trampoline(), Schedulers.trampoline());
        gameHandler.setGame(game);

        when(game.getBoard()).thenReturn(board);
    }

    @Test
    public void saveGame_savesGame() {
        gameHandler.saveGame();

        verify(gameRepository).updateGame(game);
    }

    @Test
    public void isPlayerControllable_humanPlayer_returnsTrue() {
        final Player humanPlayer = getHumanPlayer();
        when(game.getCurrentPlayer()).thenReturn(humanPlayer);

        final boolean isPlayerControllable = gameHandler.isPlayerControllable();

        assertTrue(isPlayerControllable);
    }

    @Test
    public void isPlayerControllable_aiPlayer_returnsFalse() {
        final Player aiPlayer = getAiPlayer();
        when(game.getCurrentPlayer()).thenReturn(aiPlayer);

        final boolean isPlayerControllable = gameHandler.isPlayerControllable();

        assertFalse(isPlayerControllable);
    }

    @Test
    public void newGame_createsGameWithSamePlayersAndPublishesResult() {
        final Game newGame = getGame();
        final Player whitePlayer = game.getWhitePlayer();
        final Player blackPlayer = game.getBlackPlayer();
        when(game.getWhitePlayer()).thenReturn(whitePlayer);
        when(game.getBlackPlayer()).thenReturn(blackPlayer);
        when(gameRepository.createGame(any(Player.class), any(Player.class))).thenReturn(newGame);

        final TestObserver<Game> gameObserver = new TestObserver<>();
        gameHandler.getGameObservable().subscribe(gameObserver);
        gameHandler.newGame();

        verify(gameRepository).createGame(whitePlayer, blackPlayer);
        Assert.assertEquals(newGame, gameHandler.getGame());
        assertLastEmmitedGame(gameObserver, newGame);
    }

    @Test
    public void playTile_playsTileAndPublishesResult() throws InvalidMoveException,
            WrongPlayerTurnException {
        final BoardPosition boardPosition = getBoardPosition();
        final Player currentPlayer = getWhitePlayer();
        when(game.getCurrentPlayer()).thenReturn(currentPlayer);

        final TestObserver<Game> gameObserver = new TestObserver<>();
        gameHandler.getGameObservable().subscribe(gameObserver);
        gameHandler.playTile(boardPosition);

        verify(game).playTurn(currentPlayer.getColor(), boardPosition);
        gameObserver.assertNoErrors();
        gameObserver.assertValues(game, game);
    }

    @Test
    public void passTurn_passesTurnAndPublishesResult() {
        final Player player = getWhitePlayer();
        when(game.getCurrentPlayer()).thenReturn(player);

        final TestObserver<Game> gameObserver = new TestObserver<>();
        gameHandler.getGameObservable().subscribe(gameObserver);
        gameHandler.passTurn();

        verify(game).pass(player.getColor());
        gameObserver.assertNoErrors();
        gameObserver.assertValues(game, game);
    }

    @Test
    public void resign_resignsGameAndPublishesResult() {
        final Player player = getWhitePlayer();
        when(game.getCurrentPlayer()).thenReturn(player);

        final TestObserver<Game> gameObserver = new TestObserver<>();
        gameHandler.getGameObservable().subscribe(gameObserver);
        gameHandler.resign();

        verify(game).resign(player.getColor());
        assertLastEmmitedGame(gameObserver, game);
    }

    @Test
    public void hint_returnsHint() {
        final Player currentPlayer = getWhitePlayer();
        final Turn turn = getPlayTurn();
        when(game.getCurrentPlayer()).thenReturn(currentPlayer);
        when(minimax.runAI(any(TileColor.class), any(AIConfig.class), any(Board.class)))
                .thenReturn(turn);

        final TestObserver<Turn> subscriber = new TestObserver<>();
        gameHandler.hint().subscribe(subscriber);

        subscriber.awaitTerminalEvent();
        subscriber.assertNoErrors();
        final Turn result = subscriber.values().get(0);
        Assert.assertEquals(result, turn);
        verify(minimax).runAI(currentPlayer.getColor(), aiConfig, game.getBoard());
    }

    @Test
    public void hint_calledTwice_cachesHint() {
        final Player currentPlayer = getHumanPlayer();
        final Turn turn = getPlayTurn();
        when(game.getCurrentPlayer()).thenReturn(currentPlayer);
        when(minimax.runAI(any(TileColor.class), any(AIConfig.class), any(Board.class)))
                .thenReturn(turn);

        final TestObserver<Turn> subscriber = new TestObserver<>();
        gameHandler.hint().subscribe(subscriber);
        final Turn firstHint = subscriber.values().get(0);
        gameHandler.hint().subscribe(subscriber);
        final Turn secondHint = subscriber.values().get(0);

        assertEquals(firstHint, turn);
        assertEquals(secondHint, turn);
        verify(minimax).runAI(currentPlayer.getColor(), aiConfig, game.getBoard());
    }

    @Test
    public void playTile_removesCachedHint() {
        final TestObserver<Turn> subscriber = new TestObserver<>();
        gameHandler.hint().subscribe(subscriber);

        assertFalse(gameHandler.hintIsCached());
    }

    @Test
    public void isValidMove_validMoveForCurrentPlayer_returnsTrue() {
        final BoardPosition position = getBoardPosition();
        final Player player = getWhitePlayer();
        when(game.getCurrentPlayer()).thenReturn(player);
        when(board.isValidMove(player.getColor(), position)).thenReturn(true);

        final boolean result = gameHandler.isValidMove(position);

        verify(board).isValidMove(player.getColor(), position);
        assertTrue(result);
    }

    @Test
    public void isValidMove_invalidMoveForCurrentPlayer_returnsFalse() {
        final BoardPosition position = getBoardPosition();
        final Player player = getWhitePlayer();
        when(game.getCurrentPlayer()).thenReturn(player);
        when(board.isValidMove(player.getColor(), position)).thenReturn(false);

        final boolean result = gameHandler.isValidMove(position);

        verify(board).isValidMove(player.getColor(), position);
        assertFalse(result);
    }

    @Test
    public void getAvailableMoves_returnsPossibleMoves() {
        final Player player = getWhitePlayer();
        final List<BoardPosition> moves = new ArrayList<>();
        moves.add(getBoardPosition());
        moves.add(getBoardPosition());
        when(game.getCurrentPlayer()).thenReturn(player);
        when(board.getPossibleMoves(player.getColor())).thenReturn(moves);

        final List<BoardPosition> resultingMoves = gameHandler.getAvailableMoves();

        assertEquals(moves, resultingMoves);
        verify(board).getPossibleMoves(player.getColor());
    }

    @Test
    public void removeGame_deletesGame() {
        gameHandler.removeGame();

        verify(gameRepository).removeGame(game);
    }

    @Test
    public void processTurn_humanTurn_doesNothing() {
        final Player humanPlayer = getHumanPlayer();
        when(game.getCurrentPlayer()).thenReturn(humanPlayer);

        gameHandler.processTurn();

        verify(minimax, never()).runAI(humanPlayer.getColor(), humanPlayer.getAiConfig(), board);
    }

    @Test
    public void processTurn_aiTurn_runsAi() {
        final Player aiPlayer = getAiPlayer();
        final Turn turn = getPlayTurn();
        when(game.getCurrentPlayer()).thenReturn(aiPlayer);
        when(minimax.runAI(any(TileColor.class), any(AIConfig.class), any(Board.class)))
                .thenReturn(turn);

        gameHandler.processTurn();

        verify(minimax).runAI(aiPlayer.getColor(), aiPlayer.getAiConfig(), board);
    }

    @Test
    public void processTurn_aiTurnAndError_publishesError() {
        final Player aiPlayer = getAiPlayer();
        final Throwable error = new IllegalArgumentException();
        when(game.getCurrentPlayer()).thenReturn(aiPlayer);
        when(minimax.runAI(any(TileColor.class), any(AIConfig.class), any(Board.class)))
                .thenThrow(error);

        final TestObserver<Game> gameObserver = new TestObserver<>();
        gameHandler.getGameObservable().subscribe(gameObserver);
        gameHandler.processTurn();

        gameObserver.assertError(error);
    }

    @Test
    public void runAi_playTurn_playsTurn() throws InvalidMoveException, WrongPlayerTurnException {
        final Player aiPlayer = getAiPlayer();
        final Turn turn = getPlayTurn();

        final TestObserver<Game> gameObserver = new TestObserver<>();
        gameHandler.getGameObservable().subscribe(gameObserver);
        runAiTurn(aiPlayer, turn);

        verify(game).playTurn(aiPlayer.getColor(), turn.getMove());
        assertLastEmmitedGame(gameObserver, game);
    }

    @Test
    public void runAi_passTurn_passesTurn() {
        final Player aiPlayer = getAiPlayer();
        final Turn turn = getPassTurn();

        final TestObserver<Game> gameObserver = new TestObserver<>();
        gameHandler.getGameObservable().subscribe(gameObserver);
        runAiTurn(aiPlayer, turn);

        verify(game).pass(getAiPlayer().getColor());
        assertLastEmmitedGame(gameObserver, game);
    }

    @Test
    public void runAi_resign_resigns() {
        final Player aiPlayer = getAiPlayer();
        final Turn turn = getResignTurn();

        final TestObserver<Game> gameObserver = new TestObserver<>();
        gameHandler.getGameObservable().subscribe(gameObserver);
        runAiTurn(aiPlayer, turn);

        verify(game).resign(aiPlayer.getColor());
        assertLastEmmitedGame(gameObserver, game);
    }

    private void runAiTurn(Player player, Turn turn) {
        when(game.getCurrentPlayer()).thenReturn(player);
        when(minimax.runAI(player.getColor(), player.getAiConfig(), board)).thenReturn(turn);

        gameHandler.processTurn();
    }

    private BoardPosition getBoardPosition() {
        return new BoardPosition(1, 3);
    }

    private Player getWhitePlayer() {
        return new Player("White", TileColor.WHITE);
    }

    private Player getBlackPlayer() {
        return new Player("Black", TileColor.BLACK);
    }

    private Player getHumanPlayer() {
        return getWhitePlayer();
    }

    private Player getAiPlayer() {
        final AIConfig aiConfig = new AIConfig();
        final Player player = new Player("Ai", TileColor.WHITE);
        player.setAIConfig(aiConfig);
        return player;
    }

    private Game getGame() {
        return new Game(getWhitePlayer(), getBlackPlayer());
    }

    private Turn getPlayTurn() {
        return new Turn(TileColor.WHITE, Turn.TurnType.PLAY, getBoardPosition());
    }

    private Turn getPassTurn() {
        return new Turn(TileColor.WHITE, Turn.TurnType.PASS);
    }

    private Turn getResignTurn() {
        return new Turn(TileColor.WHITE, Turn.TurnType.RESIGN);
    }

    private void assertLastEmmitedGame(TestObserver<Game> observer, final Game expectedGame) {
        observer.assertNoErrors();
        observer.assertValues(game, expectedGame);
    }

}
