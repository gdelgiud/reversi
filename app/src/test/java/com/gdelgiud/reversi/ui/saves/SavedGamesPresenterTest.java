package com.gdelgiud.reversi.ui.saves;

import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.reversi.domain.GameRepository;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SavedGamesPresenterTest {

    @Mock
    private GameRepository gameRepository;
    @Mock
    private SavedGamesView savedGamesView;

    private SavedGamesPresenter savedGamesPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        savedGamesPresenter = new SavedGamesPresenter(gameRepository);
        savedGamesPresenter.attachView(savedGamesView);
    }

    @Test
    public void viewResumed_hasNoSavedGames_closeUi() {
        when(gameRepository.hasGames()).thenReturn(false);

        savedGamesPresenter.viewResumed();

        verify(savedGamesView).closeUi();
    }

    @Test
    public void viewResumed_hasGames_showGameList() {
        final List<Game> gameList = getGameList();
        when(gameRepository.hasGames()).thenReturn(true);
        when(gameRepository.getGames()).thenReturn(gameList);

        savedGamesPresenter.viewResumed();

        verify(savedGamesView).setGames(gameList);
    }

    @Test
    public void gameSelected_openGameUiWithGame() {
        final Game game = getGame();

        savedGamesPresenter.selectGame(game);

        verify(savedGamesView).navigateToGameScreen(game);
    }

    @Test
    public void removeAllGames_showsRemoveAllGamesConfirmation() {
        savedGamesPresenter.removeAllGames();

        verify(savedGamesView).showDeleteGamesConfirmation();
        verify(gameRepository, never()).removeAllGames();
        verify(savedGamesView, never()).closeUi();
    }

    @Test
    public void confirmRemoveAllGames_removesAllGames() {
        savedGamesPresenter.confirmRemoveAllGames();

        verify(savedGamesView, never()).showDeleteGamesConfirmation();
        verify(gameRepository).removeAllGames();
        verify(savedGamesView).closeUi();
    }

    private Game getGame() {
        return new Game(getWhitePlayer(), getBlackPlayer());
    }

    private List<Game> getGameList() {
        final List<Game> games = new ArrayList<>();
        games.add(getGame());
        return games;
    }

    private Player getWhitePlayer() {
        return new Player("White", TileColor.WHITE);
    }

    private Player getBlackPlayer() {
        return new Player("Black", TileColor.BLACK);
    }

}
