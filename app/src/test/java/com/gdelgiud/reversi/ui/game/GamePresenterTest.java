package com.gdelgiud.reversi.ui.game;


import com.gdelgiud.jversi.Board;
import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.jversi.Turn;
import com.gdelgiud.jversi.ai.AIConfig;
import com.gdelgiud.jversi.exception.InvalidMoveException;
import com.gdelgiud.jversi.exception.WrongPlayerTurnException;
import com.gdelgiud.reversi.domain.GameHandler;
import com.gdelgiud.reversi.domain.PreferencesRepository;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GamePresenterTest {

    private GamePresenter gamePresenter;

    @Mock
    private GameView gameView;
    @Mock
    private Game game;
    @Mock
    private Board board;
    @Mock
    private Player currentPlayer;
    @Mock
    private GameHandler gameHandler;
    @Mock
    private PreferencesRepository preferencesRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(game.getCurrentPlayer()).thenReturn(currentPlayer);
        when(game.getBoard()).thenReturn(board);
        when(gameHandler.getGame()).thenReturn(game);

        gamePresenter = new GamePresenter(gameHandler, preferencesRepository);
        gamePresenter.attachView(gameView);
    }

    @Test
    public void viewResumed_showsGame() {
        final Player whitePlayer = getWhitePlayer();
        final Player blackPlayer = getBlackPlayer();
        when(game.getWhitePlayer()).thenReturn(whitePlayer);
        when(game.getBlackPlayer()).thenReturn(blackPlayer);
        when(gameHandler.getGameObservable()).thenReturn(Observable.just(game));

        gamePresenter.viewResumed();

        verify(gameHandler).getGameObservable();
        verify(gameView).showBoard(game.getBoard());
        verify(gameView).showWhitePlayer(whitePlayer);
        verify(gameView).showBlackPlayer(blackPlayer);
    }

    @Test
    public void subscribeToGame_outOfMemoryError_showOutOfMemoryErrorAndCloseUi() {
        final Throwable error = new OutOfMemoryError();
        final Observable<Game> gameObservable = Observable.error(error);
        when(gameHandler.getGameObservable()).thenReturn(gameObservable);

        final TestObserver<Game> gameSubscriber = new TestObserver<>();
        gameObservable.subscribe(gameSubscriber);
        gamePresenter.viewResumed();

        verify(gameView).showOutOfMemoryError();
        verify(gameView).closeUi();
    }

    @Test
    public void subscribeToGame_unkownError_showUnkownErrorAndCloseUi() {
        final Throwable error = new IllegalArgumentException();
        final Observable<Game> gameObservable = Observable.error(error);
        when(gameHandler.getGameObservable()).thenReturn(gameObservable);

        final TestObserver<Game> gameSubscriber = new TestObserver<>();
        gameObservable.subscribe(gameSubscriber);
        gamePresenter.viewResumed();

        verify(gameView).showUnknownError();
        verify(gameView).closeUi();
    }

    @Test
    public void selectTile_humanTurn_highlightsTile() {
        when(gameHandler.isPlayerControllable()).thenReturn(true);

        gamePresenter.selectTile(1, 3);

        verify(gameView).highlightTile(1, 3);
    }

    @Test
    public void selectTile_aiTurn_doesNothing() {
        when(gameHandler.isPlayerControllable()).thenReturn(false);

        gamePresenter.selectTile(1, 3);

        verify(gameView, never()).highlightTile(any(Integer.class), any(Integer.class));
    }

    @Test
    public void releaseTile_humanPlayerAndValidMove_playsTile() throws InvalidMoveException,
            WrongPlayerTurnException {
        final BoardPosition boardPosition = getBoardPosition();
        when(gameHandler.isPlayerControllable()).thenReturn(true);
        when(gameHandler.isValidMove(boardPosition)).thenReturn(true);

        gamePresenter.releaseTile(boardPosition.x, boardPosition.y);

        verify(gameHandler).playTile(boardPosition);
    }

    @Test
    public void releaseTile_humanPlayerAndInvalidMove_doesNothing() {
        final BoardPosition boardPosition = getBoardPosition();
        when(gameHandler.isPlayerControllable()).thenReturn(true);
        when(gameHandler.isValidMove(boardPosition)).thenReturn(false);

        gamePresenter.releaseTile(boardPosition.x, boardPosition.y);

        verify(gameHandler, never()).playTile(boardPosition);
    }

    @Test
    public void releaseTile_removesHighlight() {
        final BoardPosition boardPosition = getBoardPosition();

        gamePresenter.releaseTile(boardPosition.x, boardPosition.y);

        verify(gameView).removeHighlight();
    }

    @Test
    public void cancelMove_removesHighlight() {
        gamePresenter.cancelMove();

        verify(gameView).removeHighlight();
    }

    @Test
    public void showGame_humanTurn_showsPlayerControls() {
        final Player currentPlayer = getWhitePlayer();

        when(gameHandler.isPlayerControllable()).thenReturn(true);
        when(game.getCurrentPlayer()).thenReturn(currentPlayer);

        gamePresenter.showGame(game);

        verify(gameView).showPlayerControls();
    }

    @Test
    public void showGame_humanTurnAndAvailableMovesAndAvailableMovesEnabled_showAvailableMoves() {
        final Player currentPlayer = getWhitePlayer();
        final List<BoardPosition> availableMoves = getAvailableMoves();
        when(game.getCurrentPlayer()).thenReturn(currentPlayer);
        when(gameHandler.isPlayerControllable()).thenReturn(true);
        when(gameHandler.getAvailableMoves()).thenReturn(availableMoves);
        when(preferencesRepository.isShowAvailableMovesEnabled()).thenReturn(true);

        gamePresenter.showGame(game);

        verify(gameView).showAvailableMoves(availableMoves, currentPlayer.getColor());
    }

    @Test
    public void showGame_humanTurnAndAvailableMovesAndAvailableMovesDisabled_showAvailableMoves() {
        final Player currentPlayer = getWhitePlayer();
        final List<BoardPosition> availableMoves = getAvailableMoves();
        when(game.getCurrentPlayer()).thenReturn(currentPlayer);
        when(gameHandler.isPlayerControllable()).thenReturn(true);
        when(gameHandler.getAvailableMoves()).thenReturn(availableMoves);
        when(preferencesRepository.isShowAvailableMovesEnabled()).thenReturn(false);

        gamePresenter.showGame(game);

        verify(gameView, never()).showAvailableMoves(availableMoves, currentPlayer.getColor());
    }

    @Test
    public void showGame_humanTurnAndAvailableMoves_enablesHint() {
        final List<BoardPosition> availableMoves = getAvailableMoves();
        when(gameHandler.isPlayerControllable()).thenReturn(true);
        when(gameHandler.getAvailableMoves()).thenReturn(availableMoves);

        gamePresenter.showGame(game);

        verify(gameView).setHintEnabled(true);
    }

    @Test
    public void showGame_humanTurnAndNoAvailableMoves_disablesHint() {
        final List<BoardPosition> availableMoves = Collections.emptyList();
        when(gameHandler.isPlayerControllable()).thenReturn(true);
        when(gameHandler.getAvailableMoves()).thenReturn(availableMoves);

        gamePresenter.showGame(game);

        verify(gameView).setHintEnabled(false);
    }

    @Test
    public void showGame_whitePlayerTurn_showWhitePlayerTurn() {
        final Player whitePlayer = getWhitePlayer();
        when(game.getCurrentPlayer()).thenReturn(whitePlayer);

        gamePresenter.showGame(game);

        verify(gameView).showWhitePlayerActive();
    }

    @Test
    public void showGame_blackPlayerTurn_showBlackPlayerTurn() {
        final Player blackPlayer = getBlackPlayer();
        when(game.getCurrentPlayer()).thenReturn(blackPlayer);

        gamePresenter.showGame(game);

        verify(gameView).showBlackPlayerActive();
    }

    @Test
    public void showGame_aiTurn_showsWaitForAiAndClearAvailableMoves() {
        final Player aiPlayer = getAiPlayer();
        final List<BoardPosition> availableMoves = getAvailableMoves();
        when(game.getCurrentPlayer()).thenReturn(aiPlayer);
        when(gameHandler.isPlayerControllable()).thenReturn(false);
        when(gameHandler.getAvailableMoves()).thenReturn(availableMoves);

        gamePresenter.showGame(game);

        verify(gameView, never()).showAvailableMoves(availableMoves, aiPlayer.getColor());
        verify(gameView).showWaitingForAi(aiPlayer.getName());
    }

    @Test
    public void showGame_gameNotFinished_processesNextTurn() {
        when(game.isFinished()).thenReturn(false);

        gamePresenter.showGame(game);

        verify(gameHandler).processTurn();
    }

    @Test
    public void showHint_humanTurnAndPlayTurnHint_showsHintPosition() {
        final Turn hintResult = getPlayTurn();
        final Single<Turn> hintObservable = Single.just(hintResult);
        when(gameHandler.isPlayerControllable()).thenReturn(true);
        when(gameHandler.hint()).thenReturn(hintObservable);

        gamePresenter.showHint();

        verify(gameView).showHint(hintResult.getMove());
    }

    @Test
    public void passTurn_humanTurn_showsConfirmation() {
        when(gameHandler.isPlayerControllable()).thenReturn(true);

        gamePresenter.passTurn();

        verify(gameView).showPassConfirmation();
        verify(gameHandler, never()).passTurn();
    }

    @Test
    public void confirmPassTurn_humanTurn_passesTurn() {
        when(gameHandler.isPlayerControllable()).thenReturn(true);

        gamePresenter.confirmPassTurn();

        verify(gameHandler).passTurn();
    }

    @Test
    public void passTurn_aiTurn_doesNotShowConfirmation() {
        when(gameHandler.isPlayerControllable()).thenReturn(false);

        gamePresenter.passTurn();

        verify(gameView, never()).showPassConfirmation();
        verify(gameHandler, never()).passTurn();
    }

    @Test
    public void confirmPassTurn_aiTurn_doesNotPassTurn() {
        when(gameHandler.isPlayerControllable()).thenReturn(false);

        gamePresenter.passTurn();

        verify(gameHandler, never()).passTurn();
    }

    @Test
    public void detachView_gameNotFinished_savesGame() {
        when(game.isFinished()).thenReturn(false);

        gamePresenter.detachView();

        verify(gameHandler).saveGame();
    }

    @Test
    public void detachView_gameFinished_doesNotSaveGame() {
        when(game.isFinished()).thenReturn(true);

        gamePresenter.detachView();

        verify(gameHandler, never()).saveGame();
    }

    @Test
    public void resign_humanTurn_showsConfirmation() {
        when(gameHandler.isPlayerControllable()).thenReturn(true);

        gamePresenter.resign();

        verify(gameView).showResignConfirmation();
        verify(gameHandler, never()).resign();
    }

    @Test
    public void confirmResign_humanTurn_resigns() {
        when(gameHandler.isPlayerControllable()).thenReturn(true);

        gamePresenter.confirmResign();

        verify(gameHandler).resign();
    }

    @Test
    public void resign_aiTurn_doesNotShowConfirmation() {
        when(gameHandler.isPlayerControllable()).thenReturn(false);

        gamePresenter.resign();

        verify(gameView, never()).showResignConfirmation();
        verify(gameHandler, never()).resign();
    }

    @Test
    public void confirmResign_aiTurn_doesNotResign() {
        when(gameHandler.isPlayerControllable()).thenReturn(false);

        gamePresenter.confirmResign();

        verify(gameHandler, never()).resign();
    }

    @Test
    public void showGame_gameFinished_showsWinnerAndClearsActivePlayer() {
        final Player winningPlayer = getWhitePlayer();
        when(game.isFinished()).thenReturn(true);
        when(gameHandler.getWinningPlayer()).thenReturn(winningPlayer);

        gamePresenter.showGame(game);

        verify(gameView).playerVictory(winningPlayer.getName());
        verify(gameView).showRematchButton();
        verify(gameView).clearActivePlayer();
        verify(gameHandler).removeGame();
    }

    @Test
    public void rematch_startsNewGame() {
        gamePresenter.rematch();

        verify(gameHandler).newGame();
    }

    private Player getWhitePlayer() {
        return new Player("white", TileColor.WHITE);
    }

    private Player getBlackPlayer() {
        return new Player("black", TileColor.BLACK);
    }

    private Player getAiPlayer() {
        Player player = new Player("white", TileColor.WHITE);
        player.setAIConfig(new AIConfig());
        return player;
    }

    private BoardPosition getBoardPosition() {
        return new BoardPosition(1, 3);
    }

    private List<BoardPosition> getAvailableMoves() {
        final List<BoardPosition> availableMoves = new ArrayList<>();
        availableMoves.add(getBoardPosition());
        availableMoves.add(getBoardPosition());
        availableMoves.add(getBoardPosition());
        return availableMoves;
    }

    private Turn getPlayTurn() {
        return new Turn(TileColor.WHITE, Turn.TurnType.PLAY, getBoardPosition());
    }

}
