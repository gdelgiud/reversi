package com.gdelgiud.reversi.ui.main;

import com.gdelgiud.reversi.domain.GameRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MainPresenterTest {

    @Mock
    private GameRepository gameRepository;
    @Mock
    private MainView mainView;

    private MainPresenter mainPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mainPresenter = new MainPresenter(gameRepository);
        mainPresenter.attachView(mainView);
    }

    @Test
    public void viewResumed_hasSavedGames_enableSavedGamesButton() {
        when(gameRepository.hasGames()).thenReturn(true);

        mainPresenter.viewResumed();

        verify(mainView).setSavedGamesEnabled(true);
    }

    @Test
    public void viewResumed_hasNoSavedGames_disableSavedGamesButton() {
        when(gameRepository.hasGames()).thenReturn(false);

        mainPresenter.viewResumed();

        verify(mainView).setSavedGamesEnabled(false);
    }

    @Test
    public void newSinglePlayerGame_opensSinglePlayerUi() {
        mainPresenter.newSinglePlayerGame();

        verify(mainView).navigateToNewSinglePlayerGame();
    }

    @Test
    public void newTwoPlayersGame_opensTwoPlayerUi() {
        mainPresenter.newTwoPlayersGame();

        verify(mainView).navigateToNewTwoPlayersGame();
    }

    @Test
    public void watchGame_opensWatchGameUi() {
        mainPresenter.watchGame();

        verify(mainView).navigateToWatchGame();
    }

    @Test
    public void openSavedGames_hasSavedGames_opensSavedGamesUi(){
        when(gameRepository.hasGames()).thenReturn(true);

        mainPresenter.openSavedGames();

        verify(mainView).navigateToSavedGames();
    }

    @Test
    public void openSavedGames_hasNoSavedGames_doesNotOpenSavedGamesUi(){
        when(gameRepository.hasGames()).thenReturn(false);

        mainPresenter.openSavedGames();

        verify(mainView, never()).navigateToSavedGames();
    }

}
