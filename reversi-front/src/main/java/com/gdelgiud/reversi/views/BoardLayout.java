package com.gdelgiud.reversi.views;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gdelgiud.jversi.Board;
import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.TileColor;

import java.util.List;

public class BoardLayout extends LinearLayout {

    private TileView highlightedTile;

    private BoardEventListener eventListener;

    public interface BoardEventListener {
        void onTileSelected(BoardPosition position);
        void onBoardReleased();
        void onCancel();
    }

    public BoardLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public BoardLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BoardLayout(Context context) {
        super(context);
        init();
    }

    private class BoardTouchListener implements OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent event) {
            if (eventListener == null) {
                return false;
            }

            int x = (int) event.getX();
            int y = (int) event.getY();
            int row = (int) (event.getY() / getTileHeight());
            int col = (int) (event.getX() / getTileWidth());
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_MOVE:
                    if ((col >= 0 && col < Board.BOARD_WIDTH)
                            && (row >= 0 && row < Board.BOARD_HEIGHT)) {
                        eventListener.onTileSelected(new BoardPosition(col, row));
                    }
                    else {
                        eventListener.onCancel();
                    }
                    break;
                case MotionEvent.ACTION_OUTSIDE:
                case MotionEvent.ACTION_UP:
                    eventListener.onBoardReleased();
                    break;
            }
            return true;
        }
    }

    protected void init() {
        setOrientation(VERTICAL);

        for (int row = 0; row < Board.BOARD_HEIGHT; row++) {
            LinearLayout rowLayout = new LinearLayout(getContext());
            for (int col = 0; col < Board.BOARD_WIDTH; col++) {
                final TileView tile = new TileView(getContext());

                // Determine cell color
                if (row % 2 == 0) {
                    // Even row
                    if (col % 2 == 0) {
                        // Even column
                        tile.setBackgroundType(TileView.BACKGROUND_DARK);
                    }
                    else {
                        // Odd column
                        tile.setBackgroundType(TileView.BACKGROUND_LIGHT);
                    }
                }
                else {
                    // Odd row
                    if (col % 2 == 0) {
                        // Even column
                        tile.setBackgroundType(TileView.BACKGROUND_LIGHT);
                    }
                    else {
                        // Odd column
                        tile.setBackgroundType(TileView.BACKGROUND_DARK);
                    }
                }

                LayoutParams params;
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1);
                }
                else {
                    params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                }
                rowLayout.addView(tile, params);
            }

            rowLayout.setOrientation(HORIZONTAL);

            LayoutParams params;
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            }
            else {
                params = new LayoutParams(LayoutParams.MATCH_PARENT, 0, 1);
            }
            addView(rowLayout, params);
        }

        setOnTouchListener(new BoardTouchListener());

        if (isInEditMode()) {
            setBoard(new Board());
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int size;
        if(widthMode == MeasureSpec.EXACTLY && widthSize > 0){
            size = widthSize;
        }
        else if(heightMode == MeasureSpec.EXACTLY && heightSize > 0){
            size = heightSize;
        }
        else{
            size = widthSize < heightSize ? widthSize : heightSize;
        }

        int finalMeasureSpec = MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY);
        super.onMeasure(finalMeasureSpec, finalMeasureSpec);
    }

    public void setBoard(@NonNull final Board board) {
        for (int i = 0; i < Board.BOARD_HEIGHT * Board.BOARD_WIDTH; i++) {
            int row = i / 8;
            int col = i % 8;
            TileColor color = board.getTileColor(col, row);
            TileView tileView = getTileAt(col, row);

            tileView.setColor(color);
        }
    }

    public void displayValidMoves(@NonNull final Board board, @NonNull final TileColor color) {
        // Draw valid moves
        List<BoardPosition> validMoves = board.getPossibleMoves(color);

        for (BoardPosition p : validMoves) {
            TileView tile = getTileAt(p.x, p.y);
            tile.setPossibleTile();
        }
    }

    public int getTileWidth() {
        ViewGroup row = (ViewGroup) getChildAt(0);
        View tile = row.getChildAt(0);
        int width = 0;

        if (tile != null) {
            width = tile.getWidth();
        }

        return width;
    }

    public int getTileHeight() {
        View tile = getChildAt(0);
        int height = 0;

        if (tile != null) {
            height = tile.getHeight();
        }

        return height;
    }

    public TileView getTileAt(int col, int row) {
        LinearLayout rowLayout = (LinearLayout) getChildAt(row);
        return (TileView) rowLayout.getChildAt(col);
    }

    public void highlightTile(int col, int row) {
        TileView selectedTile = getTileAt(col, row);

        if (selectedTile != null && selectedTile != highlightedTile) {
            selectedTile.highlight();
            if (highlightedTile != null) {
                highlightedTile.removeHighlight();
            }
            highlightedTile = selectedTile;
        }
    }

    public void removeHighlight() {
       if (highlightedTile != null) {
           highlightedTile.removeHighlight();
           highlightedTile = null;
       }
    }

    public void setBoardEventListener(BoardEventListener eventListener) {
        this.eventListener = eventListener;
    }
}
