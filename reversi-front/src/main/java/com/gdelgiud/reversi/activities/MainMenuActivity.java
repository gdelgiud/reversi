package com.gdelgiud.reversi.activities;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;

import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.manager.GameManager;

import net.vrallev.android.task.Task;
import net.vrallev.android.task.TaskExecutor;
import net.vrallev.android.task.TaskResult;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainMenuActivity extends ReversiBaseActivity {

    @Inject
    GameManager gameManager;

    @Bind(R.id.saved_games)
    protected Button savedGamesButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        ButterKnife.bind(this);

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        savedGamesButton.setEnabled(gameManager.hasGames());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                SettingsActivity.showActivity(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.main_menu_activity;
    }

    @Override
    public void configureActionBar(@NonNull ActionBar actionBar) {
        super.configureActionBar(actionBar);
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    public void init() {

        final LoadGamesTask loadGamesTask = new LoadGamesTask(gameManager);
        TaskExecutor.getInstance().execute(loadGamesTask, this);
    }

    @TaskResult
    public void onGamesLoaded(Void nothing) {
        savedGamesButton.setEnabled(gameManager.hasGames());
    }

    @OnClick(R.id.single_player)
    public void singlePlayerClicked() {
        NewGameActivity.showActivity(this, NewGameActivity.GAME_SINGLE_PLAYER);
    }

    @OnClick(R.id.two_players)
    public void twoPlayersClicked() {
        NewGameActivity.showActivity(this, NewGameActivity.GAME_TWO_PLAYERS);
    }

    @OnClick(R.id.watch)
    public void watchClicked() {
        NewGameActivity.showActivity(this, NewGameActivity.GAME_WATCH);
    }

    @OnClick(R.id.saved_games)
    public void savedGames() {
        SavedGamesActivity.showActivity(this);
    }

    @OnClick(R.id.settings)
    public void settingsClicked() {
        SettingsActivity.showActivity(this);
    }

    public static class LoadGamesTask extends Task<Void> {

        private final GameManager gameManager;

        public LoadGamesTask(@NonNull final GameManager gameManager) {
            this.gameManager = gameManager;
        }

        @Override
        protected Void execute() {
            gameManager.loadGames();
            return null;
        }
    }
}
