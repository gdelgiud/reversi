package com.gdelgiud.reversi.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ViewSwitcher;

import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.adapters.GameListAdapter;
import com.gdelgiud.reversi.manager.GameManager;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.views.decoration.DividerItemDecoration;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SavedGamesActivity extends ReversiBaseActivity implements
        GameListAdapter.OnGameItemClickListener{

    private static final int CONTENT_INDEX = 0;
    private static final int LOADING_INDEX = 1;

    @Inject
    GameManager gameManager;

    @Bind(R.id.gameList)
    protected RecyclerView gameList;
    @Bind(R.id.contentViewSwitcher)
    protected ViewSwitcher contentViewSwitcher;

    private GameListAdapter gameListAdapter;

    public static void showActivity(final Context context) {
        Intent intent = new Intent(context, SavedGamesActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        ButterKnife.bind(this);

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameListAdapter.setGames(gameManager.getGames());
        gameListAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.delete_games_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.delete_games:
                onDeleteAllGamesSelected();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.saved_games_activity;
    }

    protected void init() {
        gameListAdapter = new GameListAdapter();
        gameListAdapter.setOnGameItemClickListener(this);
        gameList.setLayoutManager(new LinearLayoutManager(this));
        gameList.setAdapter(gameListAdapter);
        gameList.addItemDecoration(new DividerItemDecoration(this, null, false, true));
    }

    protected void onDeleteAllGamesSelected() {
        DialogInterface.OnClickListener dialogClickListener =new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        final int items = gameListAdapter.getGames().size();
                        gameManager.removeAllGames();
                        gameListAdapter.getGames().clear();
                        gameListAdapter.notifyItemRangeRemoved(0, items);
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.clear_saved_games_confirmation)
                .setPositiveButton(R.string.clear, dialogClickListener)
                .setNegativeButton(R.string.cancel, dialogClickListener).show();
    }

    @Override
    public void onGameSelected(final Game game) {
        GameActivity.showActivity(this, game);
    }
}
