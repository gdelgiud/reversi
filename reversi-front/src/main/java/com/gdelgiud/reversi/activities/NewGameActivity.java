package com.gdelgiud.reversi.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.ReversiConstants;
import com.gdelgiud.reversi.manager.GameManager;
import com.gdelgiud.reversi.model.Difficulty;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;
import com.gdelgiud.reversi.model.PlayerType;
import com.gdelgiud.reversi.views.PlayerSetupView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewGameActivity extends ReversiBaseActivity {

    public static final String GAME_TYPE_EXTRA = "game_type_extra";

    public static final int GAME_SINGLE_PLAYER = 0;
    public static final int GAME_TWO_PLAYERS = 1;
    public static final int GAME_WATCH = 2;

    @Inject
    protected GameManager gameManager;

    @Bind(R.id.player_1_setup)
    protected PlayerSetupView playerSetup1;
    @Bind(R.id.player_2_setup)
    protected PlayerSetupView playerSetup2;

    private SharedPreferences prefs;

    @Override
    public int getLayoutRes() {
        return R.layout.new_game_activity;
    }

    public static void showActivity(final Context context, int gameType) {
        Intent intent = new Intent(context, NewGameActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.putExtra(GAME_TYPE_EXTRA, gameType);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        getComponent().inject(this);

        prefs = getSharedPreferences(ReversiConstants.LAST_GAME_VALUES_PREFERENCES,
                Context.MODE_PRIVATE);

        if (savedInstanceState != null) {
            handleArguments(savedInstanceState);
        }
        else {
            handleArguments(getIntent().getExtras());
        }

        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_settings:
                SettingsActivity.showActivity(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void handleArguments(final Bundle args) {
        if (args != null && args.containsKey(GAME_TYPE_EXTRA)) {
            switch (args.getInt(GAME_TYPE_EXTRA)) {
                case GAME_SINGLE_PLAYER:
                    setUpSinglePlayer();
                    break;
                case GAME_TWO_PLAYERS:
                    setUpTwoPlayers();
                    break;
                case GAME_WATCH:
                    setUpWatch();
                    break;
            }
        }
    }

    protected void init() {
        playerSetup1.generateRandomName();
        playerSetup2.generateRandomName();

        playerSetup1.setCallback(new PlayerSetupView.Callback() {
            @Override
            public void onColorSelected(TileColor color) {
                playerSetup2.setPlayerColor(color.opposing());
            }
        });

        playerSetup2.setCallback(new PlayerSetupView.Callback() {
            @Override
            public void onColorSelected(TileColor color) {
                playerSetup1.setPlayerColor(color.opposing());
            }
        });
    }

    public void setUpSinglePlayer() {
        final String p2Diff = prefs.getString(ReversiConstants.DIFFICULTY_P2_KEY, null);

        playerSetup1.setTitle(R.string.you);
        playerSetup2.setTitle(R.string.opponent);
        playerSetup1.setPlayerColor(TileColor.WHITE);
        playerSetup2.setPlayerColor(TileColor.BLACK);
        playerSetup1.setPlayerType(PlayerType.HUMAN);
        playerSetup2.setPlayerType(PlayerType.COMPUTER);
        playerSetup1.setName(prefs.getString(ReversiConstants.LAST_NAME_P1_KEY, ""));

        if (p2Diff != null) {
            playerSetup2.setComputerDifficulty(Difficulty.valueOf(p2Diff));
        }
        else {
            playerSetup2.setComputerDifficulty(Difficulty.MEDIUM);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(R.string.single_player);
        }
    }

    public void setUpTwoPlayers() {
        playerSetup1.setTitle(R.string.player1);
        playerSetup2.setTitle(R.string.player2);
        playerSetup1.setPlayerColor(TileColor.WHITE);
        playerSetup2.setPlayerColor(TileColor.BLACK);
        playerSetup1.setPlayerType(PlayerType.HUMAN);
        playerSetup2.setPlayerType(PlayerType.HUMAN);
        playerSetup1.setName(prefs.getString(ReversiConstants.LAST_NAME_P1_KEY, ""));
        playerSetup2.setName(prefs.getString(ReversiConstants.LAST_NAME_P2_KEY, ""));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(R.string.two_players);
        }
    }

    public void setUpWatch() {
        final String p1Diff = prefs.getString(ReversiConstants.DIFFICULTY_P1_KEY, null);
        final String p2Diff = prefs.getString(ReversiConstants.DIFFICULTY_P2_KEY, null);

        playerSetup1.setTitle(R.string.player1);
        playerSetup2.setTitle(R.string.player2);
        playerSetup1.setPlayerColor(TileColor.WHITE);
        playerSetup2.setPlayerColor(TileColor.BLACK);
        playerSetup1.setPlayerType(PlayerType.COMPUTER);
        playerSetup2.setPlayerType(PlayerType.COMPUTER);
        playerSetup1.setComputerDifficulty(Difficulty.MEDIUM);
        playerSetup2.setComputerDifficulty(Difficulty.MEDIUM);

        if (p1Diff != null) {
            playerSetup1.setComputerDifficulty(Difficulty.valueOf(p1Diff));
        }
        else {
            playerSetup1.setComputerDifficulty(Difficulty.MEDIUM);
        }

        if (p2Diff != null) {
            playerSetup2.setComputerDifficulty(Difficulty.valueOf(p2Diff));
        }
        else {
            playerSetup2.setComputerDifficulty(Difficulty.MEDIUM);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(R.string.watch);
        }
    }

    @OnClick(R.id.start_game_button)
    protected void startGameButtonClicked() {
        final Player player1 = new Player(playerSetup1.getName(), playerSetup1.getPlayerColor());
        final Player player2 = new Player(playerSetup2.getName(), playerSetup2.getPlayerColor());
        Game game;

        player1.setAIConfig(playerSetup1.getAiConfig());
        player2.setAIConfig(playerSetup2.getAiConfig());
        player1.setType(playerSetup1.getPlayerType());
        player2.setType(playerSetup2.getPlayerType());
        player1.setDifficulty(playerSetup1.getDifficulty());
        player2.setDifficulty(playerSetup2.getDifficulty());

        if (player1.getColor() == TileColor.WHITE) {
            game = new Game(player1, player2);
        }
        else {
            game = new Game(player2, player1);
        }

        gameManager.addGame(game);
        persistValues();
        GameActivity.showActivity(this, game);
    }

    protected void persistValues() {
        // Persist player 1 data
        if (playerSetup1.getPlayerType() == PlayerType.COMPUTER) {
            prefs.edit().putString(ReversiConstants.DIFFICULTY_P1_KEY,
                    playerSetup1.getDifficulty().name()).apply();
        }
        else {
            prefs.edit().putString(ReversiConstants.LAST_NAME_P1_KEY,
                    playerSetup1.getName(false)).apply();
        }

        // Persist player 2 data
        if (playerSetup2.getPlayerType() == PlayerType.COMPUTER) {
            prefs.edit().putString(ReversiConstants.DIFFICULTY_P2_KEY,
                    playerSetup2.getDifficulty().name())
                    .apply();
        } else {
            prefs.edit().putString(ReversiConstants.LAST_NAME_P2_KEY,
                    playerSetup2.getName(false)).apply();
        }
    }
}
