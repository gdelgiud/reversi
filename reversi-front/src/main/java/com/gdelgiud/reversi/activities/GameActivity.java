package com.gdelgiud.reversi.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.gdelgiud.jversi.BoardPosition;
import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.jversi.Turn;
import com.gdelgiud.jversi.ai.AIConfig;
import com.gdelgiud.jversi.ai.Minimax;
import com.gdelgiud.jversi.exception.InvalidMoveException;
import com.gdelgiud.jversi.exception.WrongPlayerTurnException;
import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.ReversiConstants;
import com.gdelgiud.reversi.manager.GameManager;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;
import com.gdelgiud.reversi.utils.Utils;
import com.gdelgiud.reversi.views.BoardLayout;
import com.gdelgiud.reversi.views.PlayerStatusView;

import net.vrallev.android.task.Task;
import net.vrallev.android.task.TaskExecutor;
import net.vrallev.android.task.TaskResult;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GameActivity extends ReversiBaseActivity implements BoardLayout.BoardEventListener {

    private static final String TAG = "GameActivity";

    public static final String GAME_EXTRA = "game_extra";

    private static final String AI_TASK_ID_KEY = "ai_task_id_key";
    private static final String AI_TASK_PLAY_KEY = "ai_task_play_key";
    private static final String AI_TASK_HINT_KEY = "ai_task_hint_key";

    private static final int PLAYER_TURN_INDEX = 0;
    private static final int COMPUTER_TURN_INDEX = 1;

    @Inject
    protected GameManager gameManager;

    protected SharedPreferences prefs;

    @Bind(R.id.board)
    protected BoardLayout boardLayout;
    @Bind(R.id.whitePlayerStatus)
    protected PlayerStatusView whitePlayerStatus;
    @Bind(R.id.blackPlayerStatus)
    protected PlayerStatusView blackPlayerStatus;
    @Bind(R.id.playButton)
    protected Button playButton;
    @Bind(R.id.hintButton)
    protected Button hintButton;
    @Bind(R.id.passButton)
    protected  Button passButton;
    @Bind(R.id.resignButton)
    protected Button resignButton;
    @Bind(R.id.rematchButton)
    protected Button rematchButton;
    @Bind(R.id.bottomBarViewSwitcher)
    protected ViewSwitcher bottomBarViewSwitcher;
    @Bind(R.id.aiPlayingText)
    protected TextView aiPlayingText;

    private Game game;
    private BoardPosition selectedTilePosition;
    private Minimax minimax;
    private AIConfig hintAIConfig;
    private int aiTaskId = -1;
    private boolean hintUsed = false;
    private Turn cachedHint;

    public static void showActivity(final Context context, final Game game) {
        Intent intent = new Intent(context, GameActivity.class);
        intent.putExtra(GAME_EXTRA, game);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        getComponent().inject(this);

        if (savedInstanceState != null) {
            readArgs(savedInstanceState);
        }
        else {
            handleArguments(getIntent().getExtras());
        }

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshScreen();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameManager.updateGame(game);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Task task = TaskExecutor.getInstance().getTask(aiTaskId);
        if (task != null && task.isExecuting()) {
            task.cancel();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_settings:
                SettingsActivity.showActivity(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(AI_TASK_ID_KEY, aiTaskId);
        outState.putSerializable(GAME_EXTRA, game);
    }

    private void handleArguments(final Bundle args) {
        if (args != null && args.containsKey(GAME_EXTRA)) {
            game = (Game) args.get(GAME_EXTRA);
        }
        else {
            onGameLoadError();
        }
    }

    private void readArgs(Bundle savedInstanceState) {
        int taskId = savedInstanceState.getInt(AI_TASK_ID_KEY, -1);
        TaskExecutor.getInstance().getTask(taskId);
        handleArguments(savedInstanceState);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.game_activity;
    }

    private void init() {
        if (game == null) {
            onGameLoadError();
            return;
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(getString(R.string.players_versus,
                    game.getWhitePlayer().getName(), game.getBlackPlayer().getName()));
        }

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        refreshPlayerStatus();

        boardLayout.setBoardEventListener(this);
        minimax = new Minimax();
        hintAIConfig = new AIConfig();
        hintAIConfig.setDepthLimited(1);

        startGame();
    }

    public void startGame() {
        refreshButtonBar();
    }

    public void restartGame() {
        gameManager.removeGame(game);
        game = new Game(game.getWhitePlayer(), game.getBlackPlayer());
        gameManager.addGame(game);
        startGame();
        refreshScreen();
    }

    public void refreshScreen() {
        final Player currentPlayer = game.getCurrentPlayer();
        boardLayout.setBoard(game.getBoard());
        refreshPlayerStatus();
        refreshButtonBar();

        if (!game.isFinished()) {
            if (currentPlayer != null && currentPlayer.isControllable()) {
                if (prefs.getBoolean(ReversiConstants.SHOW_AVAILABLE_MOVES_KEY,
                        ReversiConstants.SHOW_AVAILABLE_MOVES_DEFAULT)) {
                    boardLayout.displayValidMoves(game.getBoard(), game.getCurrentColor());
                }
            }
            else {
                refreshAI();
            }
        }
    }

    public void refreshPlayerStatus() {
        whitePlayerStatus.setPlayer(game.getWhitePlayer());
        blackPlayerStatus.setPlayer(game.getBlackPlayer());

        whitePlayerStatus.setActive(game.getCurrentColor() == TileColor.WHITE && !game.isFinished());
        blackPlayerStatus.setActive(game.getCurrentColor() == TileColor.BLACK && !game.isFinished());
    }

    public void refreshAI() {
        Player currentPlayer = game.getCurrentPlayer();
        if (!game.isFinished() && currentPlayer != null && !currentPlayer.isControllable()) {
            runAI();
        }
    }

    public void refreshButtonBar() {
        Player currentPlayer = game.getCurrentPlayer();
        if (!game.isFinished()) {
            if (game.getCurrentPlayer().isControllable()) {
                showBottomBar(PLAYER_TURN_INDEX);
                if (prefs.getBoolean(ReversiConstants.CONFIRM_MOVE_KEY,
                        ReversiConstants.CONFIRM_MOVE_DEFAULT)) {
                    playButton.setVisibility(View.VISIBLE);
                    playButton.setEnabled(selectedTilePosition != null
                            && game.getBoard()
                            .isValidMove(game.getCurrentColor(),selectedTilePosition));
                }
                else {
                    playButton.setVisibility(View.GONE);
                }
                hintButton.setVisibility(View.VISIBLE);
                passButton.setVisibility(View.VISIBLE);
                resignButton.setVisibility(View.VISIBLE);
                rematchButton.setVisibility(View.GONE);

                hintButton.setEnabled(!game.getBoard().getPossibleMoves(game.getCurrentColor())
                        .isEmpty());
            }
            else {
                showBottomBar(COMPUTER_TURN_INDEX);
                aiPlayingText.setText(getString(R.string.its_players_turn,
                        game.getCurrentPlayer().getName()));
            }
        } else {
            showBottomBar(PLAYER_TURN_INDEX);

            playButton.setVisibility(View.GONE);
            hintButton.setVisibility(View.GONE);
            passButton.setVisibility(View.GONE);
            resignButton.setVisibility(View.GONE);
            rematchButton.setVisibility(View.VISIBLE);
        }
    }

    private void showBottomBar(int type) {
        if (bottomBarViewSwitcher.getDisplayedChild() != type) {
            bottomBarViewSwitcher.setDisplayedChild(type);
        }
    }

    public void runAI() {
        final AiTask task = new AiTask(minimax, game, game.getCurrentPlayer().getAiConfig());
        aiTaskId = TaskExecutor.getInstance().execute(task, this, AI_TASK_PLAY_KEY);
    }

    public void playTile(final BoardPosition boardPosition) {
        try {
            game.playTurn(game.getCurrentColor(), boardPosition);
            hintUsed = false;
            cachedHint = null;
            if (game.isFinished()) {
                onGameFinished(game.getWinningPlayer());
            }
            else {
                refreshScreen();
            }
        } catch (InvalidMoveException | WrongPlayerTurnException e) {
            e.printStackTrace();
        }
    }

    public void passTurn() {
        boardLayout.removeHighlight();
        game.switchTurn();
        hintUsed = false;
        cachedHint = null;
        refreshScreen();
    }

    public void resignGame() {
        if (game.getCurrentPlayer().isControllable()) {
            game.resign(game.getCurrentColor());
        }
        else if (game.getPlayer(game.getCurrentColor().opposing()).isControllable()) {
            game.resign(game.getCurrentColor().opposing());
        }
        else {
            Log.w(TAG, "Tried to resign a game with no human players");
        }
        onGameFinished();
    }

    public void onGameFinished(TileColor winner) {
        onGameFinished();

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.match_ended);
        if (winner == null) {
            builder.setMessage(R.string.match_ended_draw);
        }
        else {
            builder.setMessage(getString(R.string.last_turn_won, game.getPlayer(winner).getName()));
        }
        builder.setPositiveButton(R.string.ok, null).show();
    }

    public void onGameFinished() {
        gameManager.removeGame(game);
        boardLayout.setBoard(game.getBoard());
        whitePlayerStatus.setActive(false);
        blackPlayerStatus.setActive(false);
        refreshScreen();
    }

    public void onGameLoadError() {
        Log.e(TAG, "Attemted to load a activity with no game defined");
        Utils.showLongToast(getApplicationContext(), R.string.game_load_error);
        finish();
    }

    @OnClick(R.id.playButton)
    public void playButtonClicked() {
        if (game.getCurrentPlayer().isControllable()) {
            if (selectedTilePosition != null &&
                    game.getBoard().isValidMove(game.getCurrentColor(), selectedTilePosition)) {
                playTile(selectedTilePosition);
            }
        }
        boardLayout.removeHighlight();
    }

    @OnClick(R.id.hintButton)
    public void showHint() {
        if (!hintUsed && cachedHint == null) {
            final AiTask task = new AiTask(minimax, game, hintAIConfig);
            aiTaskId = TaskExecutor.getInstance().execute(task, this, AI_TASK_HINT_KEY);
            hintUsed = true;
        }
        else {
            onHintResult(cachedHint);
        }
    }

    @OnClick(R.id.passButton)
    public void passButtonClicked() {
        DialogInterface.OnClickListener dialogClickListener =new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        passTurn();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.pass_turn)
            .setMessage(R.string.are_you_sure)
            .setPositiveButton(R.string.yes, dialogClickListener)
            .setNegativeButton(R.string.no, dialogClickListener).show();
    }

    @OnClick(R.id.resignButton)
    public void resignButtonClicked() {
        DialogInterface.OnClickListener dialogClickListener =new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        resignGame();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.resign)
            .setMessage(R.string.are_you_sure)
            .setPositiveButton(R.string.yes, dialogClickListener)
            .setNegativeButton(R.string.no, dialogClickListener).show();
    }

    @OnClick(R.id.rematchButton)
    public void rematchButtonClicked() {
        restartGame();
    }

    @TaskResult(id = AI_TASK_PLAY_KEY)
    @SuppressWarnings("unused")
    public void onAiPlayed(final Turn result) {
        switch (result.getType()) {
            case PLAY:
                playTile(result.getMove());
                break;
            case PASS:
                passTurn();
                break;
            case RESIGN:
                break;
        }
    }

    @TaskResult(id = AI_TASK_HINT_KEY)
    public void onHintResult(final Turn result) {
        if (result.getType() == Turn.TurnType.PLAY) {
            onTileSelected(result.getMove());
        }
        cachedHint = result;
    }

    @Override
    public void onTileSelected(BoardPosition position) {
        if (!game.isFinished()) {
            boardLayout.highlightTile(position.x, position.y);
            if (game.getBoard().isValidMove(game.getCurrentColor(), position)) {
                selectedTilePosition = position;
                playButton.setEnabled(true);
            }
            else {
                selectedTilePosition = null;
                playButton.setEnabled(false);
            }
        }
        else {
            selectedTilePosition = null;
            boardLayout.removeHighlight();
        }
    }

    @Override
    public void onBoardReleased() {
        if (!game.isFinished() && !prefs.getBoolean(ReversiConstants.CONFIRM_MOVE_KEY,
                ReversiConstants.CONFIRM_MOVE_DEFAULT)) {
            playButtonClicked();
        }
    }

    @Override
    public void onCancel() {
        selectedTilePosition = null;
        playButton.setEnabled(false);
        boardLayout.removeHighlight();
    }

    public static class AiTask extends Task<Turn> {

        private final Minimax minimax;
        private final Game game;
        private final AIConfig aiConfig;

        public AiTask(final Minimax minimax, final Game game, final AIConfig aiConfig) {
            this.minimax = minimax;
            this.game = game;
            this.aiConfig = aiConfig;
        }

        @Override
        protected Turn execute() {
            return minimax.runAI(game.getCurrentColor(), aiConfig, game.getBoard());
        }
    }
}
