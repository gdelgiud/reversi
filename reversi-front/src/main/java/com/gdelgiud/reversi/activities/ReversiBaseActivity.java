package com.gdelgiud.reversi.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.ReversiApp;
import com.gdelgiud.reversi.dagger2.AppComponent;

import butterknife.ButterKnife;

public abstract class ReversiBaseActivity extends AppCompatActivity {

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final int layoutResId = getLayoutRes();
        if (layoutResId > 0) {
            setContentView(layoutResId);
        }

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            configureActionBar(getSupportActionBar());
        }
    }

    public abstract int getLayoutRes();

    public void configureActionBar(@NonNull final ActionBar actionBar) {
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public AppComponent getComponent() {
        return ((ReversiApp) getApplication()).getAppComponent();
    }
}
