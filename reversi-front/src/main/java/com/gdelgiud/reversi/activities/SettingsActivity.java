package com.gdelgiud.reversi.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.gdelgiud.reversi.R;

public class SettingsActivity extends ReversiBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.settings_activity;
    }

    public static void showActivity(final Context context) {
        Intent intent = new Intent(context, SettingsActivity.class);
        context.startActivity(intent);
    }

}
