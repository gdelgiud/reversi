package com.gdelgiud.reversi;

import android.app.Application;

import com.gdelgiud.reversi.dagger2.AppComponent;
import com.gdelgiud.reversi.dagger2.AppModule;
import com.gdelgiud.reversi.dagger2.DaggerAppComponent;
import com.squareup.leakcanary.LeakCanary;

public class ReversiApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        LeakCanary.install(this);
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}