package com.gdelgiud.reversi;

public class ReversiConstants {

    public static final String SHOW_AVAILABLE_MOVES_KEY = "show_available_moves";
    public static final boolean SHOW_AVAILABLE_MOVES_DEFAULT = true;
    public static final String CONFIRM_MOVE_KEY = "confirm_move";
    public static final boolean CONFIRM_MOVE_DEFAULT = false;

    public static final String LAST_GAME_VALUES_PREFERENCES = "last_game_values";
    public static final String LAST_NAME_P1_KEY = "last_name_p1";
    public static final String LAST_NAME_P2_KEY = "last_name_p2";
    public static final String DIFFICULTY_P1_KEY = "difficulty_p1";
    public static final String DIFFICULTY_P2_KEY = "difficulty_p2";

}
