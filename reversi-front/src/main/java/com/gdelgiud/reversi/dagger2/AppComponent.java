package com.gdelgiud.reversi.dagger2;

import com.gdelgiud.reversi.activities.GameActivity;
import com.gdelgiud.reversi.activities.MainMenuActivity;
import com.gdelgiud.reversi.activities.NewGameActivity;
import com.gdelgiud.reversi.activities.SavedGamesActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(MainMenuActivity activity);
    void inject(GameActivity activity);
    void inject(NewGameActivity activity);
    void inject(SavedGamesActivity activity);

}
