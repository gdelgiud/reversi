package com.gdelgiud.reversi.adapters;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gdelgiud.jversi.TileColor;
import com.gdelgiud.reversi.R;
import com.gdelgiud.reversi.model.Game;
import com.gdelgiud.reversi.model.Player;
import com.gdelgiud.reversi.model.PlayerType;

import java.util.List;

import butterknife.ButterKnife;

public class GameListAdapter extends RecyclerView.Adapter<GameListAdapter.GameItemViewHolder> {

    private List<Game> games;
    private OnGameItemClickListener listener;

    public interface OnGameItemClickListener {
        void onGameSelected(final Game game);
    }

    public void setGames(final List<Game> games) {
        this.games = games;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setOnGameItemClickListener(final OnGameItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public GameItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GameItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.game_list_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(GameItemViewHolder holder, int position) {
        holder.update(games.get(position));
    }

    @Override
    public int getItemCount() {
        return games.size();
    }

    public class GameItemViewHolder extends RecyclerView.ViewHolder {

        TextView whiteScore;
        TextView blackScore;
        TextView whiteName;
        TextView blackName;
        TextView whiteDifficulty;
        TextView blackDifficulty;

        public GameItemViewHolder(View itemView) {
            super(itemView);

            whiteScore = ButterKnife.findById(itemView, R.id.whiteScore);
            blackScore = ButterKnife.findById(itemView, R.id.blackScore);
            whiteName = ButterKnife.findById(itemView, R.id.whitePlayerName);
            blackName = ButterKnife.findById(itemView, R.id.blackPlayerName);
            whiteDifficulty = ButterKnife.findById(itemView, R.id.white_player_difficulty);
            blackDifficulty = ButterKnife.findById(itemView, R.id.black_player_difficulty);
        }

        public void update(final Game game) {
            final Player white = game.getWhitePlayer();
            final Player black = game.getBlackPlayer();

            // Update info
            whiteScore.setText(String.valueOf(white.getScore()));
            blackScore.setText(String.valueOf(black.getScore()));
            whiteName.setText(white.getName());
            blackName.setText(black.getName());

            if (game.getWhitePlayer().getType() == PlayerType.COMPUTER) {
                whiteDifficulty.setVisibility(View.VISIBLE);
                switch (game.getWhitePlayer().getDifficulty()) {

                    case EASY:
                        whiteDifficulty.setBackgroundResource(R.drawable.cpu_easy_shape);
                        break;
                    case MEDIUM:
                        whiteDifficulty.setBackgroundResource(R.drawable.cpu_medium_shape);
                        break;
                    case HARD:
                        whiteDifficulty.setBackgroundResource(R.drawable.cpu_hard_shape);
                        break;
                }
            }
            else {
                whiteDifficulty.setVisibility(View.GONE);
            }

            if (game.getBlackPlayer().getType() == PlayerType.COMPUTER) {
                blackDifficulty.setVisibility(View.VISIBLE);
                switch (game.getBlackPlayer().getDifficulty()) {

                    case EASY:
                        blackDifficulty.setBackgroundResource(R.drawable.cpu_easy_shape);
                        break;
                    case MEDIUM:
                        blackDifficulty.setBackgroundResource(R.drawable.cpu_medium_shape);
                        break;
                    case HARD:
                        blackDifficulty.setBackgroundResource(R.drawable.cpu_hard_shape);
                        break;
                }
            }
            else {
                blackDifficulty.setVisibility(View.GONE);
            }

            // Set current turn
            if (game.getCurrentColor() == TileColor.WHITE) {
                whiteName.setTypeface(null, Typeface.BOLD);
                blackName.setTypeface(null, Typeface.NORMAL);
            }
            else if (game.getCurrentColor() == TileColor.BLACK) {
                whiteName.setTypeface(null, Typeface.NORMAL);
                blackName.setTypeface(null, Typeface.BOLD);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onGameSelected(game);
                    }
                }
            });
        }
    }
}
