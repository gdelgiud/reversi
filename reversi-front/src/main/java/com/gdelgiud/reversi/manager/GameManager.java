package com.gdelgiud.reversi.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.gdelgiud.reversi.model.Game;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameManager {

    private static final String TAG = "GameManager";
    private static final String GAME_LIST_FILENAME = "game_list";

    private final SharedPreferences prefs;
    private final Context context;
    private final Gson gson;
    private long nextId = 0;
    private final HashMap<Long, Game> gamesList = new HashMap<>();

    public GameManager(final Context context, final Gson gson) {
        this.context = context;
        this.gson = gson;
        this.prefs = context.getSharedPreferences(GAME_LIST_FILENAME, Context.MODE_PRIVATE);
    }

    /**
     * Adds a new Game to the GameManager. A Game ID is automatically assigned.
     * @param game The game to add
     */
    public void addGame(@NonNull final Game game) {
        game.setId(nextId++);
        Log.d(TAG, "Created game with Id " + game.getId());
        persistGame(game);
    }

    /**
     * Finds a Game in this GameManager with the given Game's ID, and replaces it. Does nothing if
     * no such Game exists.
     * @param game The game to update
     * @return true if the Game was updated, false otherwise
     */
    public boolean updateGame(@NonNull final Game game) {
        if (gamesList.containsKey(game.getId())) {
            persistGame(game);
            Log.d(TAG, "Updated game with Id " + game.getId());
            return true;
        }

        Log.w(TAG, "Game with Id " + game.getId() + " not found");

        return false;
    }

    /**
     * Finds a Game in this GameManager with the given Game's ID, and removes it.
     * @param game The game to remove
     * @return true if the Game was removed, false otherwise
     */
    public boolean removeGame(@NonNull final Game game) {
        if (gamesList.containsKey(game.getId())) {
            gamesList.remove(game.getId());
            prefs.edit().remove(String.valueOf(game.getId())).apply();
            Log.d(TAG, "Removed game with Id " + game.getId());
            return true;
        }

        Log.w(TAG, "Game with Id " + game.getId() + " not found");

        return false;
    }

    /**
     * Removes all saved games from this Manager
     */
    public void removeAllGames() {
        gamesList.clear();
        prefs.edit().clear().apply();
    }

    /**
     * Reads through the saved games list and places them in the managed list. It sets the next game
     * ID as well.
     */
    public void loadGames() {
        Map<String,?> keys = prefs.getAll();

        for(Map.Entry<String,?> entry : keys.entrySet()){
            final Game game = gson.fromJson(entry.getValue().toString(), Game.class);
            gamesList.put(game.getId(), game);

            // Set the next game ID with the highest ID found
            if (game.getId() >= nextId) {
                nextId = game.getId() + 1;
            }
        }

        Log.d(TAG, "Loaded " + keys.size() + " games");
    }

    /**
     * Returns a copy of the Game List. Since this can be an expensive operation, do not call this
     * method repeatedly.
     * @return A copy of the Game List.
     */
    public List<Game> getGames() {
        final List<Game> ret = new ArrayList<>();
        ret.addAll(gamesList.values());
        return ret;
    }

    /**
     * Returns whether there are saved games or not
     */
    public boolean hasGames() {
        return !gamesList.isEmpty();
    }

    /**
     * Saves the given Game in internal storage. The Game is stored in a file, with its ID as
     * filename
     * @param game The game to save
     */
    private void persistGame(@NonNull final Game game) {
        gamesList.put(game.getId(), game);
        prefs.edit().putString(String.valueOf(game.getId()), gson.toJson(game)).apply();
    }
}
