# Reversi

The famous game, for Android. Includes an AI-controlled player with three skill levels.

## Used libraries

+ [jVersi](https://bitbucket.org/gdelgiud/jversi) (My own Reversi back-end, with AI included)
+ [Material Design](https://design.google.com/)
+ [Dagger2](https://google.github.io/dagger/)
+ [Butter Knife](http://jakewharton.github.io/butterknife/)
+ [GSON](https://github.com/google/gson)
+ [RxJava](https://github.com/ReactiveX/RxJava)
+ [RxAndroid](https://github.com/ReactiveX/RxAndroid)
+ [Leakcanary](https://github.com/square/leakcanary)

## Screenshots

![Main menu](https://bitbucket.org/repo/j4dM6x/images/403480312-main_menu.png) ![New game](https://bitbucket.org/repo/j4dM6x/images/3242643823-new_game.png) ![Board](https://bitbucket.org/repo/j4dM6x/images/193379745-board.png)

## License

```
Copyright 2016 Gustavo Del Giudice
   
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
